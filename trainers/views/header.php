<?php if (!isLoggedIn()) header("Location: " . SITE_URL); ?>
<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AbleSafety Trainer Hub</title>

    <!-- Vendor CSS -->
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/summernote/dist/summernote.css" rel="stylesheet">
    <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/chosen/chosen.css" rel="stylesheet">

    <!-- CSS -->
    <link href="<?php echo URL_CSS; ?>/app.min.1.css" rel="stylesheet">
    <link href="<?php echo URL_CSS; ?>/app.min.2.css" rel="stylesheet">
</head>
<body class = "toggled sw-toggled">
    <header id="header" <?php
    if (isAdmin()) {
        echo "style='background: #222222;'";
    }
    ?>>
        <ul class="header-inner">
            <li id="menu-trigger" data-trigger="#sidebar">
                <div class="line-wrap">
                    <div class="line top"></div>
                    <div class="line center"></div>
                    <div class="line bottom"></div>
                </div>
            </li>
            <li class="logo hidden-xs"> <a href="<?php echo SITE_URL . "/courses"; ?>">AbleSafety Trainer Hub</a> </li>
            <li class="pull-right">
                <ul class="top-menu">
                    <li id="toggle-width">
                        <div class="toggle-switch">
                            <input id="tw-switch" type="checkbox" hidden="hidden">
                            <label for="tw-switch" class="ts-helper"></label>
                        </div>
                    </li>
                    <li id="top-search"> <a class="tm-search" href=""></a> </li>
                    <li class="dropdown"> <a data-toggle="dropdown" class="tm-message" href=""><i class="tmn-counts">6</i></a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview">
                                <div class="lv-header"> Messages </div>
                                <div class="lv-body"> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/1.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">David Belle</div>
                                                <small class="lv-small">Cum sociis natoque penatibus et magnis dis parturient montes</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/2.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Jonathan Morris</div>
                                                <small class="lv-small">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/3.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Fredric Mitchell Jr.</div>
                                                <small class="lv-small">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/4.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Glenn Jecobs</div>
                                                <small class="lv-small">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/4.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Bill Phillips</div>
                                                <small class="lv-small">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small> </div>
                                        </div>
                                    </a> </div>
                                <a class="lv-footer" href="">View All</a> </div>
                        </div>
                    </li>
                    <li class="dropdown"> <a data-toggle="dropdown" class="tm-notification" href=""><i class="tmn-counts">9</i></a>
                        <div class="dropdown-menu dropdown-menu-lg pull-right">
                            <div class="listview" id="notifications">
                                <div class="lv-header"> Notification
                                    <ul class="actions">
                                        <li class="dropdown"> <a href="" data-clear="notification"> <i class="zmdi zmdi-check-all"></i> </a> </li>
                                    </ul>
                                </div>
                                <div class="lv-body"> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/1.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">David Belle</div>
                                                <small class="lv-small">Cum sociis natoque penatibus et magnis dis parturient montes</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/2.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Jonathan Morris</div>
                                                <small class="lv-small">Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/3.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Fredric Mitchell Jr.</div>
                                                <small class="lv-small">Phasellus a ante et est ornare accumsan at vel magnauis blandit turpis at augue ultricies</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/4.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Glenn Jecobs</div>
                                                <small class="lv-small">Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque</small> </div>
                                        </div>
                                    </a> <a class="lv-item" href="">
                                        <div class="media">
                                            <div class="pull-left"> <img class="lv-img-sm" src="<?php echo URL_INCLUDE; ?>/img/profile-pics/4.jpg" alt=""> </div>
                                            <div class="media-body">
                                                <div class="lv-title">Bill Phillips</div>
                                                <small class="lv-small">Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat</small> </div>
                                        </div>
                                    </a> </div>
                                <a class="lv-footer" href="">View Previous</a> </div>
                        </div>
                    </li>
                    <li class="dropdown hidden-xs"> <a data-toggle="dropdown" class="tm-task" href=""><i class="tmn-counts">2</i></a>
                        <div class="dropdown-menu pull-right dropdown-menu-lg">
                            <div class="listview">
                                <div class="lv-header"> Tasks </div>
                                <div class="lv-body">
                                    <div class="lv-item">
                                        <div class="lv-title m-b-5">HTML5 Validation Report</div>
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style="width: 95%"> <span class="sr-only">95% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                    <div class="lv-item">
                                        <div class="lv-title m-b-5">Google Chrome Extension</div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                    <div class="lv-item">
                                        <div class="lv-title m-b-5">Social Intranet Projects</div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                    <div class="lv-item">
                                        <div class="lv-title m-b-5">Bootstrap Admin Template</div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                    <div class="lv-item">
                                        <div class="lv-title m-b-5">Youtube Client App</div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="lv-footer" href="">View All</a> </div>
                        </div>
                    </li>
                    <li class="dropdown"> <a data-toggle="dropdown" class="tm-settings" href=""></a>
                        <ul class="dropdown-menu dm-icon pull-right">
                            <li class="hidden-xs"> <a data-action="fullscreen" href=""><i class="zmdi zmdi-fullscreen"></i> Toggle Fullscreen</a> </li>
                            <li> <a data-action="clear-localstorage" href=""><i class="zmdi zmdi-delete"></i> Clear Local Storage</a> </li>
                            <li> <a href=""><i class="zmdi zmdi-face"></i> Privacy Settings</a> </li>
                            <li> <a href=""><i class="zmdi zmdi-settings"></i> Other Settings</a> </li>
                        </ul>
                    </li>
                    <li class="hidden-xs" id="chat-trigger" data-trigger="#chat"> <a class="tm-chat" href=""></a> </li>
            </li>
        </ul>

        <!-- Top Search Content -->
        <div id="top-search-wrap" <?php
        if (isAdmin()) {
            echo "style='background: #222222;'";
        }
        ?>>
            <input type="text">
            <i id="top-search-close" <?php
            if (isAdmin()) {
                echo "style='color: #5e5e5e;'";
            }
            ?>>&times;</i> </div>
    </header>
    <section id="main">
        <aside id="sidebar">
            <div class="sidebar-inner c-overflow">
                <div class="profile-menu"> <a href="">
                        <div class="profile-pic"> <img src="<?php echo URL_INCLUDE; ?>/img/profile-pics/1.jpg" alt=""> </div>
                        <div class="profile-info"> <?php echo $_SESSION['user']['first_name'] . " " . $_SESSION['user']['last_name']; ?> <i class="zmdi zmdi-arrow-drop-down"></i> </div>
                    </a>
                    <ul class="main-menu">
                        <li> <a href="<?php echo '/' . $_SESSION['user']['type'] . '/profile' ?>"><i class="zmdi zmdi-account"></i> View Profile</a> </li>
                        <li> <a href="<?php echo '/' . $_SESSION['user']['type'] . '/setting' ?>"><i class="zmdi zmdi-input-antenna"></i> Privacy Settings</a> </li>
                        <li> <a href=""><i class="zmdi zmdi-settings"></i> Settings</a> </li>
                        <li> <a href="<?php echo SITE_URL . "/signout.php"; ?>"><i class="zmdi zmdi-time-restore"></i> Logout</a> </li>
                    </ul>
                </div>
                <ul class="main-menu">
                    <li class="<?php
						if (substr($_SERVER["REQUEST_URI"], 0, 10) == "/dashboard") {
							echo 'active';
						}
						?>"><a href="<?php echo SITE_URL . "/dashboard"; ?>"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
					<?php /* if (isSuperAdmin()) { ?>
                      <li class="<?php
                      if (substr($_SERVER["REQUEST_URI"], 0, 7) == "/admins") {
                      echo 'active';
                      }
                      ?>"><a href="<?php echo SITE_URL . "/admins"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Manage Admin</a></li>
                      <?php } */ ?>
                    <?php if ($_SESSION['user']['user_type'] == '0' || $_SESSION['user']['user_type'] == '2'): ?>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 8) == "/courses") {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/courses"; ?>"><i class="zmdi zmdi-assignment"></i> Courses</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 12) == "/instructors") {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/instructors"; ?>"><i class="zmdi zmdi-accounts-alt"></i> Instructors</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 9) == "/students") {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/students"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Students</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 15) == "/emailTemplates") {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/emailTemplates"; ?>"><i class="zmdi zmdi-email"></i> Email Templates</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 7) == "/orders") {
                                echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/orders"; ?>"><i class="zmdi zmdi-shopping-cart"></i>Orders</a>
                        </li>
						<li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 9) == "/invoices") {
                            echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/invoices"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Invoices</a>
                        </li>
                    <?php endif; ?>
                    
                    <?php if ($_SESSION['user']['user_type'] == '1'): ?>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 8) == "/classes") {
                            echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/classes"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Classes</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 9) == "/students") {
                            echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/students"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Students</a>
                        </li>
                        <li class="<?php
                            if (substr($_SERVER["REQUEST_URI"], 0, 9) == "/invoices") {
                            echo 'active';
                            }
                            ?>"><a href="<?php echo SITE_URL . "/invoices"; ?>"><i class="zmdi zmdi-accounts-outline"></i> Invoices</a>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </aside>
        <section id="content">
            <div class = "container">
