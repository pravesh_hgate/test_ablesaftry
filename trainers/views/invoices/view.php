<?php
require_once("../../../wp-load.php");
$id = $_SESSION['user']['trainer_id'];
if (isset($_GET['id']) && $_GET['id'] != '') {
    $invoiceId = $_GET['id'];
}
if ($_SESSION['user']['user_type'] == '0' || $_SESSION['user']['user_type'] == '2'){
	$STH = $asdb->query("select * from invoice_details where invoice_id=" . $invoiceId );
	$cat = $STH->fetchAll(PDO::FETCH_ASSOC);
	$STH1 = $asdb->query("select * from invoices where id=" . $invoiceId );
	$invoiceData = $STH1->fetch(PDO::FETCH_ASSOC);
	
	$STHU = $asdb->query("select * from trainers where trainer_id=" . $invoiceData['trainer_id'] );
}else{
	$STH = $asdb->query("select * from invoice_details where invoice_id=" . $invoiceId . " and trainer_id=" . $id);
	$cat = $STH->fetchAll(PDO::FETCH_ASSOC);
	$STH1 = $asdb->query("select * from invoices where id=" . $invoiceId . " and trainer_id=" . $id);
	$invoiceData = $STH1->fetch(PDO::FETCH_ASSOC);
	$STHU = $asdb->query("select * from trainers where trainer_id=" . $id );
}
$user= $STHU->fetch(PDO::FETCH_ASSOC);
$weekData = x_week_range($invoiceData['date_time']);

$trVal = "";
$grandTotal = 0;
$totalHours=0;
foreach ($cat as $val) {
    $timeFidd = date("Mj", strtotime($val['class_start_date'])) . " " . "(" . date("h:i A", strtotime($val['class_start_date'])) . '-' . date("h:i A", strtotime($val['class_end_date'])) . ")";
    $rowTotal = ($val['total_hours'] * $val['hourly_rate']);
    $sthC = $asdb->query("Select name from osha_terms INNER JOIN osha_term_taxonomy on osha_term_taxonomy.term_id=osha_terms.term_id INNER JOIN osha_term_relationships on osha_term_relationships.term_taxonomy_id=osha_term_taxonomy.term_taxonomy_id where osha_term_relationships.object_id=" . $val['course_id']);
    $result1 = $sthC->fetchColumn();
    $trVal.='<tr><td width="20%"><h5 class="text-uppercase f-400">' . $result1 . '</h5></td><td>' . $timeFidd . '</td><td>' . $val['total_hours'] . '</td><td>$' . $val['hourly_rate'] . '</td><td class="highlight">$' . $rowTotal . '</td></tr>';
    $grandTotal+=$rowTotal;
    $totalHours+=$val['total_hours'];
}
?>

<!--<div class="container invoice">-->

<div class="block-header">
    <h2>Invoice Detail
<!--            <small>Print ready simple and sleek invoice template. Please use Google Chrome or any other
            Webkit browsers for better printing.
        </small>-->
    </h2>
</div>

<!--[if IE 9 ]>
    <div class="alert alert-warning"><i class="zmdi zmdi-alert-triangle"></i> Please note that Printing is not supported in 1E 9</div>
<![endif]-->

<div class="card">
    <div class="card-header ch-alt text-center">
        <img class="i-logo" src="<?php echo URL_INCLUDE; ?>/img/ablesafety.png" alt="">
    </div>

    <div class="card-body card-padding">
        <div class="row m-b-25">
            <div class="col-xs-6">
                <div class="text-right">
                    <p class="c-gray">Invoice from</p>

                    <h4>Able Safety Consulting</h4>

                    <span class="text-muted">
                        <address>
                            124 Front Street Suite<br> 207 Massapequa Park<br> NY 11762
                        </address>
                        888-926-4727<br/>
                        info@able-safety.com
                    </span>
                </div>
            </div>

            <div class="col-xs-6">
                <div class="i-to">
                    <p class="c-gray">Invoice to</p>

                    <h4><?php echo $user['first_name'] . " " . $user['last_name']; ?></h4>

                    <span class="text-muted">
                        <address>
                            <?php echo $user['address']; ?><br>
                            <?php echo $user['city']; ?> <br> <?php echo $user['state']; ?> <?php echo $user['zip']; ?>
                        </address>

                        <?php echo $user['phone']; ?><br/>
                        <?php echo $user['email']; ?>
                    </span>
                </div>
            </div>

        </div>

        <div class="clearfix"></div>

        <div class="row m-t-25 p-0 m-b-25">
            <div class="col-xs-3">
                <div class="bgm-amber brd-2 p-15">
                    <div class="c-white m-b-5">Invoice#</div>
                    <h2 class="m-0 c-white f-300"><?php echo  $invoiceData['randomid']; ?></h2>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="bgm-blue brd-2 p-15">
                    <div class="c-white m-b-5">Date</div>
                    <h2 class="m-0 c-white f-300"><?php echo date("Mj", strtotime($weekData[0])) . '-' . date("Mj", strtotime($weekData[1])); ?></h2>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="bgm-green brd-2 p-15">
                    <div class="c-white m-b-5">Total Hours</div>
                    <h2 class="m-0 c-white f-300"><?php echo $totalHours;?></h2>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="bgm-red brd-2 p-15">
                    <div class="c-white m-b-5">Grand Total</div>
                    <h2 class="m-0 c-white f-300">$<?php echo round($grandTotal, 2); ?></h2>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <table class="table i-table m-t-25 m-b-25">
            <thead class="text-uppercase">
            <th class="c-gray">Class Name</th>
            <th class="c-gray">Date</th>
            <th class="c-gray">Total hours worked</th>
            <th class="c-gray">Hourly Rate</th>
            <th class="highlight">TOTAL</th>
            </thead>

            <tbody>
            <thead>
                <?php echo $trVal; ?>
                <tr>
                    <td colspan="3"></td>
                    <td>Total</td>
                    <td class="highlight">$<?php echo round($grandTotal, 2); ?></td>
                </tr>
            </thead>
            </tbody>
        </table>

        <div class="clearfix"></div>

        <!--            <div class="p-25">
                        <h4 class="c-green f-400">REMARKS</h4>
                        <p class="c-gray">Ornare non tortor. Nam quis ipsum vitae dolor porttitor interdum.
                            Curabitur faucibus erat vel ante fermentum lacinia. Integer porttitor laoreet
                            suscipit. Sed cursus cursus massa ut pellentesque. Phasellus vehicula dictum arcu,
                            eu interdum massa bibendum.</p>

                        <br/>

                        <h4 class="c-green f-400">MERCY FOR YOUR BUSINESS</h4>
                        <p class="c-gray">Proin ac iaculis metus. Etiam nisi nulla, fermentum blandit
                            consectetur sed, ornare non tortor. Nam quis ipsum vitae dolor porttitor interdum.
                            Curabitur faucibus erat vel ante fermentum lacinia. Integer porttitor laoreet
                            suscipit. Sed cursus cursus massa ut pellentesque. Phasellus vehicula dictum arcu,
                            eu interdum massa bibendum sit amet.</p>
                    </div>
                </div>

                <footer class="m-t-15 p-20">
                    <ul class="list-inline text-center list-unstyled">
                        <li class="m-l-5 m-r-5">
                            <small>support@company.com</small>
                        </li>
                        <li class="m-l-5 m-r-5">
                            <small>00971 452 9900</small>
                        </li>
                        <li class="m-l-5 m-r-5">
                            <small>www.company.com</small>
                        </li>
                    </ul>
                </footer>-->
    </div>

</div>

<!--<a class="btn btn-float bgm-red m-btn" data-ma-action="print" href=""><i class="zmdi zmdi-print"></i></a>-->



