<?php

require_once("../../../wp-load.php");
if(isset($_POST['post_data'])){

global $wpdb;
	$exist = $wpdb->get_var("SELECT COUNT(*) FROM orders WHERE course_id = ".$_POST['course_id']);
if($exist==0){
$wpdb->query( "DELETE FROM  osha_posts WHERE ID =".$_POST['course_id']);
$wpdb->query( "DELETE FROM osha_postmeta  WHERE post_id =".$_POST['course_id']);

echo 1;

}
else{ echo 0; die;}
}
getheader();


if ($_GET['id']) {
    $id = $_GET['id'];

    $STH = $asdb->query('SELECT name FROM osha_terms INNER JOIN osha_term_taxonomy ON osha_terms.term_id = osha_term_taxonomy.term_id WHERE osha_term_taxonomy.term_taxonomy_id =' . $id);
    $cat = $STH->fetch(PDO::FETCH_ASSOC);
}
?>
<script>
    function back() {
        location.href = '<?php echo SITE_URL;?>/courses/';
    }
</script>
<style>.validate{color:#F00;}</style>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1><?php echo $cat['name']; ?></h1>
</div>
<div class="card">
    <div class="card-header">

        <div class="row">
            <div class="col-sm-6">
                <h2>Public <small>Manage course under this specific category.</small></h2>
            </div>
            <div class="col-sm-6">
                <a data-toggle="modal" href="<?php echo SITE_URL;?>/courses/addevent?id=<?php echo $id; ?>" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Class</a>
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table id="public_course_table" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id" data-type="numeric" data-order="desc">#</th>
<!--                    <th data-column-id="name">Name</th>-->
                    <th data-column-id="starttime" data-converter="starttime">Start Time</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php echo category_courses($id); ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="publicClass" tabindex="-1" role="dialog" aria-hidden="true">
    <form class="form-horizontal" role="form" name="add-public-class" id="add-public-class">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Class</h4>
                </div>
                <div class="modal-body">
                    <div class="card-body card-padding">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="title" id="title" value="<?php echo $cat['name']; ?>">
                                    <input type="hidden" name="category_id" value="<?php echo $id; ?>">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Venue Name</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="venue_name" id="venue_name" placeholder="Venue Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="address" id="venue_address" placeholder="Address" required="true">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">City</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="city" placeholder="City" id="venue_city" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">State</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select name = "state" class="form-control pclass" id="venue_state">
                                        <option value = "" disabled selected>State</option>
                                        <?php
                                        foreach ($us_states_abbrev as $abbrev => $state) {
                                            echo "<option value = '" . $abbrev . "'>" . $abbrev . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Zip</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="zip" placeholder="Zip" id="venue_zip" required="true" digits="true">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date and Time</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control date-picker" id="date" name="schedule_date" placeholder="Click here..." required="true" >

                                    From:

                                    <input type="text" class="form-control time-picker" id="startTime" name="startTime"  placeholder="Click here...">

                                    To:
                                    <input type="text" class="form-control time-picker" id = "endTime" name="endTime"  placeholder="Click here...">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select class="from-control pclass" name="dateList" id="dateList" size="5" style="height:100px; width: 100%;" multiple=""></select>
                                </div>
                            </div>
                            <a id="removeFromList" class="col-sm-2 btn btn-primary btn-sm waves-effect">Remove</a>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a id = "addDate" class="btn btn-primary btn-sm waves-effect">Add</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button  type="submit" class="btn btn-link" name="schedule_class">Schedule  Class</button>
                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-6">
                <h2>Private <small>Manage course under this specific category.</small></h2>
            </div>
            <div class="col-sm-6">
                <a data-toggle="modal" href="<?php echo SITE_URL;?>/courses/addprivateevent?id=<?php echo $id; ?>" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Class</a>
            </div>
        </div>
    </div>
    <div class="table-responsive">
        <table id="private_course_table" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id" data-type="numeric" data-order="desc">#</th>
<!--                    <th data-column-id="name">Name</th>-->
                    <th data-column-id="starttime" data-converter="starttime">Start Time</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php echo category_courses($id, true); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h2>Expired Courses <small>Manage course under this specific category.</small></h2>
    </div>
    <div class="table-responsive">
        <table id="expired_course_table" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id" data-type="numeric" data-order="desc">#</th>
<!--                    <th data-column-id="name">Name</th>-->
                    <th data-column-id="starttime" data-converter="starttime">Start Time</th>
                    <th data-column-id="classtype">Type</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php echo expired_category_courses($id); ?>
            </tbody>
        </table>
    </div>
</div>

<div class="modal fade" id="modalWider" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Class</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method ="post" action="">
                    <div class="card-body card-padding">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="title" value="<?php echo $cat['name']; ?>">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Venue Name</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="venue" placeholder="Venue Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Address</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="address" placeholder="Address">
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">City</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="city" placeholder="City">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">State</label>
                            <div class="col-sm-4">

                                <div class="fg-line">

                                    <select name = "state" class="form-control pclass">
                                        <option value = "" disabled selected>State</option>
                                        <?php
                                        foreach ($us_states_abbrev as $abbrev => $state) {
                                            echo "<option value = '" . $abbrev . "'>" . $abbrev . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <label class="col-sm-2 control-label">Zip</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="zip" placeholder="Zip">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Date and Time</label>
                            <div class="col-sm-4">

                                <div class="fg-line">
                                    <input type="text" class="form-control date-picker" id="date" name="date" placeholder="Click here...">

                                    From:

                                    <input type="text" class="form-control time-picker" id="startTime" name="startTime" placeholder="Click here...">

                                    To:
                                    <input type="text" class="form-control time-picker" id = "endTime" name="endTime" placeholder="Click here...">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <select class="from-control pclass" name="dateList" id="dateList" size="5" style="height:100px; width: 100%;" multiple=""></select>
                                </div>
                            </div>
                            <a id="removeFromList" class="col-sm-2 btn btn-primary btn-sm waves-effect">Remove</a>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a id = "addDate" class="btn btn-primary btn-sm waves-effect">Add</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button onclick="submit(this)" id = "addClass" type="button" class="btn btn-link">Schedule  Class</button>
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
getfooter();
?>
<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function () {
        //Basic Example
        $("#public_course_table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {
                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/?id=" + row.id + "'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                    html += "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/addevent?id=<?php echo $id; ?>&objid=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> ";

                    html += "<button \"deleteRecord\" onclick=\"deleteClass("+row.id+");\" type=\"button\" class=\"btn bgm-gray btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"  data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";

                    return html;
                }
            },
            converters: {
                starttime: {
                    from: function (value) {
                        var test = value.split(' From');
                        return moment(test[0]) + "T" + test[1];
                    },
                    to: function (value) {
                        var test = value.split('T:');
                        return moment.unix(test[0] / 1000).format("MMM DD,YY") + " From:" + test[1];
                    }
                }
            }
        });

        //Command Buttons
        $("#private_course_table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {

                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/?pid=" + row.id + "'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                    html += "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/addprivateevent?id=<?php echo $id; ?>&pid=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> ";

                    html += "<button  id = \"deleteRecord\" onclick=\"deleteClass("+row.id+");\" type=\"button\" class=\"btn bgm-gray btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"  data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";

                    return html;
                }
            },
            converters: {
                starttime: {
                    from: function (value) {
                        var test = value.split(' From');
                        return moment(test[0]) + "T" + test[1];
                    },
                    to: function (value) {
                        var test = value.split('T:');
                        return moment.unix(test[0] / 1000).format("MMM DD,YY") + " From:" + test[1];
                    }
                }
            }
        });

        $("#expired_course_table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {

                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/?id=" + row.id + "'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                    return html;
                }
            },
            converters: {
                starttime: {
                    from: function (value) {
                        var test = value.split(' From');
                        return moment(test[0]) + "T" + test[1];
                    },
                    to: function (value) {
                        var test = value.split('T:');
                        return moment.unix(test[0] / 1000).format("MMM DD,YY") + " From:" + test[1];
                    }
                }
            }
        });

        //$("#private_course_table-header").append('<br><a data-toggle="modal" href="#modalWider" class="btn btn-sm btn-default waves-effect bgm-blue">Add Class</a>');


        $("#add-public-class").validate({
            rules: {
                venue_name: {required: true},
                title: {required: true},
                venue_address: {required: true},
                venue_city: {required: true},
                venue_state: {required: true},
                venue_zip: {required: true, digits: true},
                date: {required: true},
                startTime: {required: true},
                endTime: {required: true}
            },
            submitHandler: function (form) {

                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "<?php echo URL_PHP; ?>/courses/add_public_class.php",
                    data: $('#add-public-class').serialize(),
                    beforeSend: function () {

                    },
                    success: function (response) {
                        alert(response.message);
                    }
                });

            }
        });





    });

    function addToList()
    {
        var listElement = document.getElementById("dateList");
        var option = document.createElement("option");
        var dateElement = document.getElementById("date");
        var startTime = document.getElementById("startTime");
        var endTime = document.getElementById("endTime");
        var dateElementArray = dateElement.value.split("/");
        var fixedDateString = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];

        var dateobj = new Date(fixedDateString);
        var dateString = dateobj.toUTCString();
        var dateArray = dateString.split(" ");

        var dateTextForm = dateArray[2] + " " + dateArray[1] + ", " + dateArray[3] + " From: " + startTime.value +
                " To: " + endTime.value + "  ";

        option.text = dateTextForm;
        option.value = dateTextForm;

        listElement.add(option);


    }
	function deleteClass(id){ 
		if(confirm('Are you sure, you want to delete this record ?')){
$.ajax({
url:"category.php",
method:"POST",
data:{course_id:id,post_data:'recordCount'},
success:function(result){
if(result==0){alert('You Can Not Delete This Class.');}else{alert('Record Deleted Successfully.');location.reload(); }
}
});
		}	
	}

    $('#addDate').click(function () {
        addToList();
        return false;
    });
    $('#removeFromList').click(function () {

        var listElement = document.getElementById("dateList");
        listElement.remove(listElement.selectedIndex);
    });
    function submit(element) {

        var post_data = getValues(".pclass", element);

        $.ajax({
            url: "<?php echo URL_PHP; ?>/courses/add_private_class.ajax.php",
            type: "POST",
            data: post_data,
            success: function (html) {
                if (html == "true")
                {
                    $('.toggled').removeClass('toggled');
                    notify('Successfully Created', 'success');

                    $("#instructor-display").html($("#instructor-select option:selected").text());
                } else
                {
                    notify('Error Creating Class', 'danger');
                }

            }
        });
    }
    function getValues(selector, element) {
        var tempValues = "";
        var listElement = document.getElementById("dateList");

        for (var i = 0; i < listElement.options.length; i++)
        {
            listElement.options[i].selected = true;
        }

        $(selector).each(function () {
            var th = $(this);
            tempValues += th.attr('name') + "=" + th.val() + "&";

        });
        tempValues += "button=" + element.id + "&cat=<?php echo $_GET['id']; ?>";
        return tempValues;
    }



</script>


