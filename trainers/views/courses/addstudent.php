<?php
getheader();
?>
<style>
/*.fg-line{width:50% !important;}*/
</style>

<!--<div class="card" id="profile-main">-->
<div class="card" id="profile-main">
<!--    <div class="pm-overview c-overflow">
        <div class="pmo-pic">

        </div>

    </div>-->

    <div class="pm-body clearfix">
    
	<!--<div>-->
        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-account m-r-5"></i> Basic Information</h2>

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a data-pmb-action="edit" href="">Edit</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pmbb-body p-l-30">
                

                <div class="pmbb-edit">
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Full Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "full_name" type="text" class="form-control" value="">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "address" type="text" class="form-control" value="">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">City</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "city" type="text" class="form-control" value="<?php echo $student['city']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">State</dt>
                        <dd>
                            <div class="fg-line">
                                <select name = "state" class="form-control">
                                    <?php 
                                    
                                        foreach($us_states_abbrev as $abbrev=>$state)
                                        {
                                            echo "<option value = '".$abbrev."'>".$abbrev."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Zip</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "zip" type="text" class="form-control" value="<?php echo $student['zip']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Company Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "company_name" type="text" class="form-control" value="<?php echo $student['company_name']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <div class="m-t-30">
                        <button onclick = "submit(this)" id = "editBasic" class="btn btn-primary btn-sm">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-phone m-r-5"></i> Contact Information</h2>

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a data-pmb-action="edit" href="">Edit</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pmbb-body p-l-30">
                
                <div class="pmbb-edit">
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Phone</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "phone" type="text" class="form-control" value="<?php echo $student['phone']; ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Email Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "email" type="email" class="form-control" value="<?php echo $student['email']; ?>">
                            </div>
                        </dd>
                    </dl>
 
                    <div class="m-t-30">
                        <button onclick = "submit(this)" id = "editContact" class="btn btn-primary btn-sm">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="card">

    <div class="card-header">
        <h2>Add Student</h2>
    </div>
    
    <div class="pmb-block toggled">
            
            <div class="pmbb-body p-l-30">
                

                <div class="pmbb-edit">
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">First Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name="first_name" class="form-control"  type="text">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Last Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name="last_name" class="form-control"  type="text">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Phone No</dt>
                        <dd>
                            <div class="fg-line">
                                <input name="phone" class="form-control"   type="text">
                            </div>

                        </dd>
                    </dl>
                    
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Email Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name="email" class="form-control"  type="text">
                            </div>

                        </dd>
                    </dl>
                    
                    <div class="m-t-30">
                        <button   class="btn btn-primary btn-sm waves-effect">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm waves-effect">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    
    
    
    
    
</div>
<?php    
getfooter();
?>