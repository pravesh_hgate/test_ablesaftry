<?php

if($_GET['id'])
{

$course_id = $_GET['id'];
$private = 0;
    
$STH = $asdb->prepare("SELECT term_taxonomy_id
                            FROM osha_term_relationships
                            WHERE object_id = ?");
    
$STH->execute(array($course_id));

$cat = $STH->fetch(PDO::FETCH_ASSOC)['term_taxonomy_id'];

$STH = $asdb->prepare('SELECT post_title, osha_postmeta.meta_value
                        FROM osha_posts 
                        INNER JOIN osha_postmeta ON osha_posts.ID = osha_postmeta.post_id
                        WHERE osha_posts.ID = ? AND meta_key = "dateAndTime"');

$STH->execute(array($course_id));
$course = $STH->fetchAll();

foreach($course as $key=>$row)
{
    $dateAndTime = unserialize($row['meta_value']);
}

$students = $asdb->prepare('SELECT attendance, order_id, student_id, full_name, payment_type, course_type, balance_due, order_type, phone, email, message
                        FROM orders
                        INNER JOIN user_list ON orders.student_id = user_list.user_id
                        WHERE course_id = ? AND private = 0');



}
elseif($_GET['pid'])
{
    
$course_id = $_GET['pid'];
$private = 1;

$STH = $asdb->prepare('SELECT *
                        FROM private_courses 
                        WHERE id = ?');
    
$STH->execute(array($course_id));
$temp_course = $STH->fetchAll();
$course = array();
    
foreach($temp_course as $key=>$row)
{
    $course[$key]['post_title'] = $row['title'];
    $venue_name =  $row['venue_name'];
    $venue_address =  $row['address'];
    $venue_city =  $row['city'].", ".$row['state']." ".$row['zip'];
    $dateAndTime = unserialize($row['dateAndTime']);
    $cat = $row['category_id'];
}
    
$students = $asdb->prepare('SELECT attendance, order_id, student_id, full_name, payment_type, course_type, balance_due, order_type, phone
                        FROM orders
                        INNER JOIN user_list ON orders.student_id = user_list.user_id
                        WHERE course_id = ? AND private = 1');
}

$students->execute(array($course_id));
$container = $students->fetchAll();

$dateArray = array();
foreach($dateAndTime as $date)
{
    $explodedDate = explode(" ", $date);
    $tempArray = array( "date"  => implode(array_slice($explodedDate, 0, 3)), 
                        "start" => implode(array_slice($explodedDate, 4, 2)),
                        "end"   => implode(array_slice($explodedDate, 7, 2)));
    array_push($dateArray, $tempArray);
}

$dateCheck = array();
foreach($dateArray as $key=>$date)
{
        $dateTime = new DateTime($date['date']." ".$date["start"]);
        $newDateTime = $dateTime->add(new DateInterval('PT2H'));
        if($key == 0 && new DateTime() > $newDateTime)
        {
            
            array_push($dateCheck, 1);
        }
        elseif(new DateTime() > $dateTime)
        {
               
                array_push($dateCheck, 1);
        }
        else
        {
            array_push($dateCheck, 0);
            
        }
}



?>
<?php
/*echo "<pre>";
print_r($container);*/
?>
<div style = "width:100%;">
<div class="CSSTableGenerator" >
    <table >
        <tr>
        	<td>
                User Id
            </td>
            <td>
                Name
            </td>
            <td>
                Payment Option
            </td>
            <td>
                Balance Due
            </td>
            <td>
                Payment
            </td>
            <td>
                Phone Number
            </td>
            <td>
                Email Address
            </td>
            <td>
                Notes
            </td>
        </tr>
        
        <?php
        
            foreach($container as $student)
            {
                switch ($student['payment_type']) 
                {
                    case "online":
                        $payment_status = '<p style = "color:green;"><strong>Online CC</strong></p>';
                        break;
                    case "deposit":
                        $payment_status = '<p style = "color:Orange;"><strong>$50 Deposit</strong></p>';
                        break;
                    case "door":
                        $payment_status = '<p style = "color:red;"><strong>Unpaid</strong></p>';
                        break;
                    case "phone":
                        $payment_status = '<p style = "color:red;"><strong>Unpaid</strong></p>';
                        break;
                }
                
                switch ($student['order_type']) 
                {
                    case "student":
                        $order_type = 'Single Order';
                        break;
                    case "payer":
                        $order_type = 'Multiple Order';
                        break;
                }
                
                /*if($student['order_type'] != 'payer')
                {*/
                    echo "<tr>
					<td>
                        ".$student['student_id']."
                    </td>
                    <td>
                        ".$student['full_name']."
                    </td>";
                    
                    if($student['order_type'] == 'multiple')
                    {
                        echo "<td>Online</td><td>0</td><td> Paid by User ".$student['payment_type']."</td>";
                        
                    }
                    else
                    {
                         echo "<td>
                                ".$payment_status."
                            </td><td>
                                ".$student['balance_due']."
                            </td><td></td>";
                    }
                    

                    echo "<td>".$student['phone']."</td>";
                    echo "<td>".$student['email']."</td>";
                    echo "<td>".$student['message']."</td></tr>";
               // }
                  
                                
            }
        
        ?>
    </table>
</div>
</div>


<style>

    .CSSTableGenerator {
	margin:0px;padding:0px;
	width:100%;
	border:1px solid #000000;
	
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
	vertical-align:middle;
	
	
	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	font-size:16px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}.CSSTableGenerator tr:last-child td{
	border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
	border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
		background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");	background: -o-linear-gradient(top,#cccccc,b2b2b2);

	background-color:#cccccc;
	border:0px solid #000000;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:18px;
	font-family:Arial Black;
	font-weight:bold;
	color:#000000;
}
.CSSTableGenerator tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");	background: -o-linear-gradient(top,#cccccc,b2b2b2);

	background-color:#cccccc;
}
.CSSTableGenerator tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}
    
</style>