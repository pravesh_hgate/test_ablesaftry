<?php
getheader();

if($_GET['pid'])
{

    $course_id = $_GET['pid'];
    $STH = $asdb->prepare('SELECT *
                        FROM private_courses 
                        WHERE id = ?');

    $STH->execute(array($course_id));
    $course = $STH->fetchAll();
?>

<div class="card">
                        
    <form class="form-horizontal" role="form" method ="post" action="">
        <div class="card-header">
            <h2><?php echo $course[0]['title']; ?><small><?php echo "Start Date: ".unserialize($course[0]['dateAndTime'])[0]; ?></small></h2>
        </div>

        <div class="card-body card-padding">
            <div class="form-group">
                <label class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-4">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="fname" placeholder="First Name">
                    </div>
                </div>
                <label class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="lname" placeholder="Last Name">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="email" class="form-control input-sm" name="email" placeholder="Email">
                    </div>
                </div>
                <label class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="cname" placeholder="Company Name">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="address" placeholder="Address">
                    </div>
                </div>
                <label class="col-sm-2 control-label">City</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="city" placeholder="City">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">State</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        
                         <select name = "state" class="form-control">
                             <option value = "" disabled selected>State</option>
                             <?php 
                                foreach($us_states_abbrev as $abbrev=>$state)
                                {
                                    echo "<option value = '".$abbrev."'>".$abbrev."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <label class="col-sm-2 control-label">Zip</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                       <input type="text" class="form-control input-sm" name="fname" placeholder="Zip">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Phone Number</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="phone" placeholder="Phone Number">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm waves-effect">Register</button>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
}
getfooter();
?>
<script>
$(document).ready(function() {

    $('form').submit(function(event) {

        var formData = {
            'fname':    $('input[name=fname]').val(),
            'lname':    $('input[name=lname]').val(),
            'email':    $('input[name=email]').val(),
            'cname':    $('input[name=cname]').val(),
            'address':  $('input[name=address]').val(),
            'city':     $('input[name=city]').val(),
            'state':    $('input[name=state]').val(),
            'zip':      $('input[name=zip]').val(),
            'phone':    $('input[name=phone]').val(),
            'course_id':'<?php echo $course_id; ?>',
            'cat_id':   '<?php echo $course[0]['category_id']; ?>'
        };

        $.ajax({
            type        : 'POST', 
            url         : '<?php echo URL_PHP; ?>/courses/register-ajax.php', 
            data        : formData,
            success: function(html){
            
            }
        });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});
</script>