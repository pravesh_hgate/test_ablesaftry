<?php

if($_GET['id'])
{

	$course_id = $_GET['id'];
	$private = 0;
		
	$STH = $asdb->prepare("SELECT term_taxonomy_id
								FROM osha_term_relationships
								WHERE object_id = ?");
		
	$STH->execute(array($course_id));
	
	$cat = $STH->fetch(PDO::FETCH_ASSOC)['term_taxonomy_id'];
	
	$STH = $asdb->prepare('SELECT post_title, osha_postmeta.meta_value, osha_postmeta.meta_key 
							FROM osha_posts 
							INNER JOIN osha_postmeta ON osha_posts.ID = osha_postmeta.post_id
							WHERE osha_posts.ID = ?');
	
	$STH->execute(array($course_id));
	$course = $STH->fetchAll();
	
	foreach($course as $key=>$row)
	{
		switch ($row['meta_key']) 
		{
			case "dateAndTime":
				$dateAndTime = unserialize($row['meta_value']);
				break;
			case "_EventVenueID":
				$venueID = $row['meta_value'];
				break;
			case "_EventCost":
				$cost = $row['meta_value'];
				break;
		}
	
	}

	$STH = $asdb->prepare('SELECT osha_postmeta.meta_value, osha_postmeta.meta_key 
							FROM osha_posts 
							INNER JOIN osha_postmeta ON osha_posts.ID = osha_postmeta.post_id
							WHERE osha_posts.ID = ?');
	
	$STH->execute(array($venueID));
	$venue = $STH->fetchAll();
	
	foreach($venue as $key=>$row)
	{
		switch ($row['meta_key']) 
		{
			case "_VenueVenue":
				$venue_name = $row['meta_value'];
				break;
			case "_VenueAddress":
				$venue_address = $row['meta_value'];
				break;
			case "_VenueCity":
				$venue_city = $row['meta_value'];
				break;
		}
	
	}

	$students = $asdb->prepare('SELECT attendance, order_id, student_id, full_name, payment_type, course_type, balance_due, order_type
							FROM orders
							INNER JOIN user_list ON orders.student_id = user_list.user_id
							WHERE course_id = ? AND private = 0');

}
elseif($_GET['pid'])
{
    
	$course_id = $_GET['pid'];
	$private = 1;
	
	$STH = $asdb->prepare('SELECT *
							FROM private_courses 
							WHERE id = ?');
		
	$STH->execute(array($course_id));
	$temp_course = $STH->fetchAll();
	$course = array();
		
	foreach($temp_course as $key=>$row)
	{
		$course[$key]['post_title'] = $row['title'];
		$venue_name =  $row['venue_name'];
		$venue_address =  $row['address'];
		$venue_city =  $row['city'].", ".$row['state']." ".$row['zip'];
		$dateAndTime = unserialize($row['dateAndTime']);
		$cat = $row['category_id'];
	}
		
	$students = $asdb->prepare('SELECT attendance, order_id, student_id, full_name, payment_type, course_type, balance_due, order_type 
							FROM orders
							INNER JOIN user_list ON orders.student_id = user_list.user_id
							WHERE course_id = ? AND private = 1');
}

$students->execute(array($course_id));
$container = $students->fetchAll();

$dateArray = array();
foreach($dateAndTime as $date)
{
    $explodedDate = explode(" ", $date);
    $tempArray = array( "date"  => implode(array_slice($explodedDate, 0, 3)), 
                        "start" => implode(array_slice($explodedDate, 4, 2)),
                        "end"   => implode(array_slice($explodedDate, 7, 2)));
    array_push($dateArray, $tempArray);
}

$dateCheck = array();
foreach($dateArray as $key=>$date)
{
        $dateTime = new DateTime($date['date']." ".$date["start"]);
        $newDateTime = $dateTime->add(new DateInterval('PT2H'));
        if($key == 0 && new DateTime() > $newDateTime)
        {
            
            array_push($dateCheck, 1);
        }
        elseif(new DateTime() > $dateTime)
        {
               
                array_push($dateCheck, 1);
        }
        else
        {
            array_push($dateCheck, 0);
            
        }
}
?>

<div class="block-header">
    <button onclick="location.href='<?php echo SITE_URL;?>/courses/category?id=<?php echo $cat; ?>';" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1> <?php echo " ".$course[0]['post_title']." - ".$_GET['id']; ?> </h1>
</div>
<div class="card" id="profile-main">
    <div style = "padding:0px" class="pm-body clearfix">
        
         <div class="row">
             <div class = "col-sm-6">
                 <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-city m-r-5"></i> Location</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">

                            <dl class="dl-horizontal">
                                <dt>Venue</dt>
                                <dd><?php echo $venue_name; ?></dd>
                            </dl> 
                            <dl class="dl-horizontal">
                                <dt>Address</dt>
                                <dd><?php echo $venue_address; ?></dd>
                                <dd><?php echo $venue_city; ?></dd>
                            </dl>

                         </div>
                    </div>
                </div>
        		<div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-calendar m-r-5"></i> Schedule</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <?php foreach($dateAndTime as $key=>$date) {?>
                            
                            <dl class="dl-horizontal">
                                <dt>Day <?php echo $key+1; ?></dt>
                                <dd><?php echo $date;
                                    if($dateCheck[$key] == 1)
                                        echo '<i class="zmdi zmdi-check zmdi-hc-fw" style = "color:#4CAF50; font-size:1.5em;"></i>';
                                    ?></dd>
                            </dl>
        
                            <?php } ?>
                            
                        </div>
                    </div>
        		</div>
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-account zmdi-hc-fw"></i> Instructor</h2>
                        <ul class="actions">
                            <li class="dropdown">
                                <a href="" data-toggle="dropdown">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
        
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a data-pmb-action="edit" href="">Edit</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">                  
                            <dl class="dl-horizontal">
                                <dt>Name</dt>
                                <dd id = "instructor-display"><?php 
                                    $GCI = $asdb->query("SELECT trainers.first_name, trainers.last_name FROM course_instructor
                                                        INNER JOIN trainers ON trainers.trainer_id = course_instructor.trainer_id
                                                        WHERE course_instructor.course_id='".$_GET['id']."'");
                                    
                                    while($trainer = $GCI->fetch(PDO::FETCH_ASSOC)){
                                        echo $trainer['first_name']." ".$trainer['last_name'];
                                    }
                                    ?></dd>
                            </dl>      
                        </div>
                        <div class="pmbb-edit" p-l-30>
                        <dl class="dl-horizontal">
                            <dt>Instructor</dt>
                            <dd>
                                    <select id = 'instructor-select' name="trainer_id" class="instructor form-control">
                                        <?php 
                                            $IL = $asdb->query('SELECT * FROM trainers');
                                            foreach($IL as $trainer){
                                                echo "<option value='".$trainer['trainer_id']."'>".$trainer['first_name']." ".$trainer['last_name']."</option>";
                                            }
                                        ?>
                                    </select>
                                    <input class='form-control instructor' type="hidden" name="course_id" value="<?php echo $_GET['id']; ?>" />
        
                            </dd>
                            </dl>
                            <div class="m-t-30">
                                <button onclick = "submit(this)" id = "editContact" class="btn btn-primary btn-sm">Save</button>
                                <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-assignment-account m-r-5"></i>Registration - 
                        
                            <?php 
                            
                            if($dateCheck[0] == 1)
                            {
                                echo "<i style = 'color:#F44336'>CLOSED</i>";
                            }
                            else{
                                echo "<i style = 'color:#4CAF50'>OPEN</i>";
                            }
                            ?>
                        </h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                         <?php  
                        if($dateCheck[0] != 1){
                    ?>    
                        <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                <dt>Link</dt>
                                <dd><?php
                                
                                    if($_GET['id'])
                                    {
                                         echo "<a class='btn btn-primary btn-block' href='https://www.ablesafety.com/?post_type=tribe_events&#038;p=".$course_id."'>Registration Page on Ablesafety.com</a>";                     
                                    }
                                    elseif($_GET['pid'])
                                    {
                                         echo "<a class='btn btn-primary btn-block' href='".SITE_URL."/courses/register?pid=".$course_id."'>Private Registration page</a>";
                                    }
                                    ?>
                                </dd>
                                </dl>
                            
                        </div>
                        
                    <?php  }  ?>
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                            <dt>Roster</dt>
                            <dd><?php
                                   
                                
                                    if($_GET['id'])
                                    {
                                         echo "<a class='btn btn-primary btn-block' href='".SITE_URL."/courses/roster?id=".$course_id."'>View Printable Roster</a>";                     
                                    }
                                    elseif($_GET['pid'])
                                    {
                                         echo "<a class='btn btn-primary btn-block' href='".SITE_URL."/courses/roster?pid=".$course_id."'>View Printable Roster</a>";
                                    }
                                    ?>
                            </dd>
                            </dl>
                            
                        </div>
        
                    </div>    
        		</div>
            </div>
            <div class = "col-sm-6">
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-pin m-r-5"></i> MAP</h2>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                              <dl class="">
                            
                                <dd>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item"
                                          frameborder="0" style="border:0"
                                          src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBQ5_woe_v0bheiLESaODCOMJBYRqrAfS0&q=<?php echo $venue_address.", ".$venue_city;?>" allowfullscreen>
                                        </iframe>
                                    </div>
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
	         </div>
         </div>

    </div>
</div>

<div class="card">
    <div class="card-header">
    	<div class="row">
            <div class = "col-sm-6">
            <h2>Students </h2>
            </div>
            <div class = "col-sm-6">
            	<a data-toggle="modal" href="#modalWider" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Student</a>
          		<!--<a class="btn btn-primary btn-block waves-effect" href="<?php echo SITE_URL;?>/courses/addstudent" style="width: 120px; float:right;">Add Student</a>--> 
            </div>
        </div>
    </div>
    <table id="attendance" class="table table-striped table-vmiddle">
        <thead>
            <tr>
                <th data-column-id="order_id" data-type="numeric" data-visible = "false" data-visible-in-selection = "false" >ORDER ID</th>
                <th data-column-id="id" data-type="numeric" data-visibleInSelection = "false" data-visible = "false">ID</th>
                <th data-column-id="student">Student</th>
                
                <?php foreach($dateAndTime as $key=>$date) {?>

                <th data-column-id="<?php echo $key; ?>" data-formatter="check-box"><?php echo substr($date, 0, 12); ?></th>

                <?php } ?>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($container as $row) {
                    if(($row['order_type'] != 'payer')){
        ?>
            <tr>
                <td><?php echo $row['order_id']; ?></td>
                <td><?php echo $row['student_id']; ?></td>
                <td><?php echo $row['full_name']; ?></td>
                <?php
                                          
                    $attendance = unserialize($row['attendance']);
                    if(is_array($attendance))
                    {
                        foreach($attendance as $check)
                        {
                            echo "<td>".$check."</td>";
                        }
                        if(!in_array(1, $attendance))
                        {
                            echo "<td>0</td>";
                        }
                        elseif(!in_array(0, $attendance))
                        {
                            echo "<td>1</td>";
                        }
                    }
                    else
                    {
                        for($i = 0; $i < count($dateAndTime); $i++)
                        {
                            echo "<td>0</td>";
                        }
                      
                            echo "<td>0</td>";
                      
                    }
                   
                ?>
            </tr>
        
        <?php }}?>
         </tbody>
    </table>
</div>

<div class="modal fade" id="modalWider" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add Student</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" method ="post" action="">

                    <div class="card-body card-padding">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Full Name</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="full_name" value="Full Name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Phone</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="phone" placeholder="Phone">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-4">
                                <div class="fg-line">
                                    <input type="text" class="form-control input-sm pclass" name="email" placeholder="Email">
                                 
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button onclick="addStudent(this)" id = "addClass" type="button" class="btn btn-link">Add Student</button>
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php if(isAdmin()): ?>
<div class="card">
    <div class="card-header">
        <h2>Payment Information</h2>
    </div>

    <table id="students" class="table table-striped table-vmiddle">
        <thead>
            <tr>
                <th data-column-id="order_id" data-type="numeric" data-visible = "false" data-visible-in-selection = "false" >ORDER ID</th>
                <th data-column-id="id" data-type="numeric">ID</th>
                <th data-column-id="student">Student</th>
                <th data-column-id="payment" data-formatter="payment-selector">Payment Status</th>
                <th data-column-id="balance">Balance Due</th>
                <th data-column-id="order_type" data-formatter="order-type">Order Type</th>
                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($container as $row) {
                    if($row['order_type'] != 'multiple'){
            ?>
            <tr>
                <td><?php echo $row['order_id']; ?></td>
                <td><?php echo $row['student_id']; ?></td>
                <td><?php echo $row['full_name']; ?></td>
                <td><?php echo $row['payment_type']; ?></td>
                <td><?php echo "$".$row['balance_due']; ?></td>
                <td><?php echo $row['order_type']; ?></td>
            </tr>
            <?php }}?>
        </tbody>
    </table>
</div>



<?php
endif;
    getfooter();
?>


<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function(){


        //students table 
        $("#students").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function(column, row) { 
                    return "<button onclick=\"location.href='<?php echo SITE_URL;?>/students/?id="+row.id+"'\" type=\"button\" class=\"btn btn-icon command-edit\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> " + 
                        "<button type=\"button\" class=\"btn btn-icon command-delete\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";
                },
                
                "order-type": function(column, row) { 
                    var htmlString = "Single Order";
                    
                    if(row[column.id] == 'payer')
                        htmlString = "Multiple Order";
                    
                    return htmlString;
                },
                
                "payment-selector": function(column, row){
                    
                    var htmlString = "<div style = 'width:50%;' class = 'select'><select class = 'form-control select-payment' data-order-id = '"+row.order_id+"' data-row-id='" + row.id + "'";
                    
                    if(row[column.id] == 'online')
                    {
                        htmlString += " disabled><option value = 'online' selected>Paid</option><option value = 'deposit'>Deposit</option><option value = 'phone'>Phone</option><option value = 'door'>Door</option></select></div>";
                    }
                    else if(row[column.id] == 'deposit')
                    {
                        htmlString += "><option value = 'online'>Paid</option><option value = 'deposit' selected>Deposit</option><option value = 'phone'>Phone</option><option value = 'door'>Door</option></select></div>";
                    }
                    else if(row[column.id] == 'door')
                    {
                        htmlString += "><option value = 'online'>Paid</option><option value = 'deposit'>Deposit</option><option value = 'phone'>Phone</option><option value = 'door' selected>Door</option></select></div>";
                    }
                    else if(row[column.id] == 'phone')
                    {
                        htmlString += "><option value = 'online'>Paid</option><option value = 'deposit'>Deposit</option><option value = 'phone' selected>Phone</option><option value = 'door'>Door</option></select></div>";
                    }
                    return htmlString;
                }
            }
        });

		var course_id = <?php echo $course_id; ?>;
		var num_dates = <?php echo count($dateAndTime); ?>;
		var ajax_attendance_url = "<?php echo URL_PHP; ?>/courses/load-attendance-ajax.php";
        //attendance table
        $("#attendance").bootgrid({
            
            ajax: false,
            post: function ()
            {
                /* To accumulate custom parameter with the request object */
                return { id: course_id, num_dates: num_dates };
            },
            url: ajax_attendance_url,
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "check-box": function(column, row) { 
                    var html = "<div class='checkbox'><label><input class = 'attendanceCheck' type = 'checkbox' data-col-id = '"+column.id+"'data-order-id = '"+row.order_id+"' data-student-id = '"+row.id+"' data-col-name = '"+column.text+"' ";
                    if(row[column.id] == '1'){
                        html+="checked /><i class='input-helper'></i></label></div>";
                    }
                    else
                    {
                        html+="/><i class='input-helper'></i></label></div>";
                    }
                    return html;
                },
				

				
                "commands": function(column, row) { 
                    var html =  "<button type='button' id = 'complete"+row.order_id+"'onclick='markComplete(this)' class='markComplete bgm-green btn btn-icon waves-effect waves-circle waves-float' data-col-id = '"+column.id+"'data-row-id='" + row.id + "' data-order-id='"+row.order_id+"'"; 
                    if(row[column.id] == '1')
                    {
                        html += " ><span class='zmdi zmdi-check'></span></button> Mark Complete";
                    }
                    else
                    {
                       html += " disabled><span class='zmdi zmdi-check'></span></button> Mark Complete";
                    }
                       html += " <button type=\"button\" class=\"btn btn-icon command-delete\" data-row-id=\"" + row.id + "\" onclick=\"deleteStudent(this)\"><span class=\"zmdi zmdi-delete\"></span></button>";
                    return html;
                }


            }
        });
        
    });
    
	function addStudent(element) {
    
		var post_data = getValues(".pclass", element);

			$.ajax({
				url: "<?php echo URL_PHP; ?>/courses/add_student_class.ajax.php",
				type: "POST",
				data:  post_data,
				success: function(html){
					
					if(html == "true")
					{
						$('.toggled').removeClass('toggled');
						notify('Successfully Created', 'success');
	                    location.reload();
						/*$("#instructor-display").html( $("#instructor-select option:selected").text());*/
					}
					else
					{
						notify('Error Creating Class', 'danger');
					}
	
			   }
		});
	}
	
	
    $(document).on('change', ".select-payment",function () {
   
   
            var student_id = $(this).attr('data-row-id');
            var order_id = $(this).attr('data-order-id');
            var payment_type = $(this).val();
            
            $.ajax({
                
               type : 'post',
               url : '<?php echo URL_PHP ?>/courses/students-table-ajax.php', 
               data :  {student_id: student_id, payment_type: payment_type, order_id: order_id}, 
               success : function(html)
               {
                   if(html == "true")
                   {
                     notify("Payment Status Updated", "success");
                   }
               },
               error: function()
               {
                   notify("There was an error.", "success");
               }
           });

        
        
    });
    
    $(document).on('change', "input.attendanceCheck:checkbox",function () {


        var student_id = $(this).attr('data-student-id');
        var order_id = $(this).attr('data-order-id');
        var column_id = $(this).attr('data-col-id');
        var dateStr = $(this).attr('data-col-name');
        var dates = '<?php echo serialize($dateAndTime); ?>';
        var studentNum = <?php echo count($container); ?>;
        var datesNum = <?php echo count($dateAndTime); ?>;
     
        
        $.ajax({

           type : 'post',
           url : '<?php echo URL_PHP ?>/courses/attendance-ajax.php', 
           data :  {student_id: student_id, dateStr: dateStr, order_id: order_id, dates: dates}, 
           success : function(html)
           {
                if(html == "true")
                {
                    notify("Attendance Updated", "success");
                    // get array of rows
                    var currentRows = $('#attendance').data('.rs.jquery.bootgrid').currentRows;
                  
                    //loop through rows the row check box is in
                    for(var i = 0; i < currentRows.length; i++) 
                    {

                        var value = currentRows[i];
                        if(value.order_id == order_id)
                        {
                            var currentRow = value;
                            var rowIndex = i;
                        }
                    }
                
                    //flip digits on check box data cell            
                    if(currentRow[column_id] == 1)
                    {
                         currentRow[column_id] = 0;                       
                    }
                    else
                    {
                        currentRow[column_id] = 1;    
                    }
                    
                    //check all date columns in row to see if all are checked
                    var disabled = false;
                    for(var i = 0; i < datesNum; i++)
                    {
                        if(currentRow[i] == 0)
                        {
                            disabled = true;
                            break;
                        }
                    }
                    //disable or enable mark complete button
                    if(disabled)
                    {
                       $("#complete"+order_id).prop("disabled",true);                         
                    }
                    else
                    {
                       $("#complete"+order_id).prop("disabled",false);
                    }

                }
            },
           error: function()
           {
               notify("There was an error.", "success");
           }
       });
    });
    
    function markComplete(element) {
        
        var student_id = $(element).attr('data-row-id');
        var order_id = $(element).attr('data-order-id');
        var privateChek = <?php echo $private; ?>;
        
        $.ajax({

           type : 'post',
           url : '<?php echo URL_PHP ?>/courses/mark-complete-ajax.php', 
           data :  {student_id: student_id, order_id: order_id, private:privateChek},
           success : function(html)
           {
               if(html == "no instructor")
               {
                 notify("This course has no Instructor", "danger");
               }
               else{
                notify("Certificate Sent", "success");
               }
           },
           error: function()
           {
               notify("There was an error.", "success");
           }
       });

    }
	 function deleteStudent(element) {
        
        var student_id = $(element).attr('data-row-id');
        $.ajax({

           type : 'post',
           url : '<?php echo URL_PHP ?>/courses/delete-student-ajax.php', 
           data :  {student_id: student_id},
           success : function(html)
           {
               if(html == "true")
               {
                 notify("Student has been deleted", "success");
				 location.reload();
               }
               else{
                notify("Student not deleted", "danger");
               }
           },
           error: function()
           {
               notify("There was an error.", "success");
           }
       });

    }

function submit(element) {
    
var post_data = getValues(".instructor", element);

    $.ajax({
        url: "<?php echo URL_PHP; ?>/instructor/assign-instructor-ajax.php",
        type: "POST",
        data:  post_data,
        success: function(html){
            if(html == "true")
            {
                $('.toggled').removeClass('toggled');
                notify('Successfully Updated', 'success');
                
                $("#instructor-display").html( $("#instructor-select option:selected").text());
            }
            else
            {
                notify('Error updating', 'danger');
            }
     
       }
    });
}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id+"&course_id=<?php echo $course_id; ?>&course_type=<?php echo $cat; ?>&private=<?php echo $private; ?>";
  return tempValues;
}

    
</script>