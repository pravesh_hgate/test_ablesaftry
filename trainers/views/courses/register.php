<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AbleSafety Trainer Hub</title>

        <!-- Vendor CSS -->
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="<?php echo URL_CSS; ?>/app.min.1.css" rel="stylesheet">
        <link href="<?php echo URL_CSS; ?>/app.min.2.css" rel="stylesheet">
        
        
    </head>
<?php

if($_GET['pid'])
{

    $course_id = $_GET['pid'];
    $STH = $asdb->prepare('SELECT *
                        FROM private_courses 
                        WHERE id = ?');

    $STH->execute(array($course_id));
    $course = $STH->fetchAll();
?>

<div class="card">
                        
    <form class="form-horizontal" role="form" method ="post" action="">
        <div class="card-header">
            <h2><?php echo $course[0]['title']; ?><small><?php echo "Start Date: ".unserialize($course[0]['dateAndTime'])[0]; ?></small></h2>
        </div>

        <div class="card-body card-padding">
            <div class="form-group">
                <label class="col-sm-2 control-label">First Name</label>
                <div class="col-sm-4">
                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="fname" placeholder="First Name">
                    </div>
                </div>
                <label class="col-sm-2 control-label">Last Name</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="lname" placeholder="Last Name">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="email" class="form-control input-sm" name="email" placeholder="Email">
                    </div>
                </div>
                <label class="col-sm-2 control-label">Company Name</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="cname" placeholder="Company Name">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="address" placeholder="Address">
                    </div>
                </div>
                <label class="col-sm-2 control-label">City</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="city" placeholder="City">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">State</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        
                         <select id = 'state' name = "state" class="form-control">
                             <option value = "" disabled selected>State</option>
                             <?php 
                                foreach($us_states_abbrev as $abbrev=>$state)
                                {
                                    echo "<option value = '".$abbrev."'>".$abbrev."</option>";
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <label class="col-sm-2 control-label">Zip</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                       <input type="text" class="form-control input-sm" name="zip" placeholder="Zip">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Phone Number</label>
                <div class="col-sm-4">

                    <div class="fg-line">
                        <input type="text" class="form-control input-sm" name="phone" placeholder="Phone Number">
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm waves-effect">Register</button>
                </div>
            </div>
        </div>
    </form>
</div>

<?php
}
?>
                <footer id="footer">
            Copyright &copy; 2015 Able Safety LLC.

        </footer>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.time.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        

        <script type="text/javascript" src="<?php echo URL_JS; ?>/functions.js"></script>
        <script type="text/javascript" src="<?php echo URL_JS; ?>/custom_functions.js"></script>
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bootgrid/jquery.bootgrid.min.js"></script>
     
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo URL_INCLUDE; ?>/vendors/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        
        
    </body>
<script>
$(document).ready(function() {

    $('form').submit(function(event) {

        var formData = {
            'fname':    $('input[name=fname]').val(),
            'lname':    $('input[name=lname]').val(),
            'email':    $('input[name=email]').val(),
            'cname':    $('input[name=cname]').val(),
            'address':  $('input[name=address]').val(),
            'city':     $('input[name=city]').val(),
            'state':    $('#state option:selected').val(),
            'zip':      $('input[name=zip]').val(),
            'phone':    $('input[name=phone]').val(),
            'course_id':'<?php echo $course_id; ?>',
            'cat_id':   '<?php echo $course[0]['category_id']; ?>'
        };

        $.ajax({
            type        : 'POST', 
            url         : '<?php echo URL_PHP; ?>/courses/register-ajax.php', 
            data        : formData,
            success: function(html){
            
            }
        });

        // stop the form from submitting the normal way and refreshing the page
        event.preventDefault();
    });

});
</script>
  </html>