<?php
$STH = $asdb->query('SELECT * FROM user_list');

?>
<div class="card">
    <div class="card-header">
        <h2>Students</h2>
    </div>
                        
    <div class="table-responsive table-editable">
        <table id="data-table-selection" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="user_id" data-type="numeric" data-order="asc">#</th>
                    <th data-formatter="editable" data-column-id="full_name">Name</th>
                    <th data-formatter="editable" data-column-id="email">Email Address</th>
                    <th data-formatter="editable" data-column-id='phone'>Phone #</th>
                    <th data-formatter="editable" data-column-id='company_name'>Company</th>
                    <th data-formatter="editable" data-column-id='address'>Address</th>
                    <th data-formatter="editable" data-column-id='city'>City</th>
                    <th data-formatter="editable" data-column-id='state'>State</th>
                    <th data-formatter="editable" data-column-id='zip'>Zip</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php  
            while($users = $STH->fetch(PDO::FETCH_ASSOC)){
        echo "<tr><td>".$users['user_id']."</td>";
        echo "<td>".$users['full_name']."</td>";
        echo "<td>".$users['email']."</td>";
        echo "<td>".$users['phone']."</td>";
        echo "<td>".$users['company_name']."</td>";
        echo "<td>".$users['address']."</td>";
        echo "<td>".$users['city']."</td>";
        echo "<td>".$users['state']."</td>";
        echo "<td>".$users['zip']."</td></tr>";
    
                }
                ?>  
            </tbody>
        </table>
    </div>
</div>
<?php getfooter(); ?>
<style>

    .edit{
        text-decoration: none;
        border: 0px;
        color: rgb(94,94,94);
    }
    
</style>
<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function(){
        $.fn.editable.defaults.mode = 'inline';
        //Selection
        $("#data-table-selection").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            selection: true,
            multiSelect: true,
            rowSelect: true,
            keepSelection: true,
             formatters: {
                "editable": function(column, row) { 
                    
                    return '<a href="" style = "border-bottom: 0px;" class="edit" data-type="text" data-name="'+column.id+'" data-pk="'+row['user_id']+'" data-url="<?php echo URL_PHP; ?>/students/students.ajax.php">'+row[column.id]+'</a>';                    
                    
                },
                 "commands": function(column, row) {
                    return "<button onclick=\"location.href='<?php echo SITE_URL;?>/students/?id="+row.user_id+"'\" class=\"btn btn-info waves-effect waves-float\" data-row-id=\"" + row.user_id + "\">More Info</button> ";
                }
                
             }
        }).on("loaded.rs.jquery.bootgrid", function (e){
              $('.edit').each(function(column, row){    
                $(this).editable();
              })
             });

    });
    
</script>