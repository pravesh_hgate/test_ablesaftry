<?php
ob_start();
getheader();
?>
<?php
require_once("../../../wp-load.php");
if ($_GET['id']) {
    $id = $_GET['id'];
    $query = "SELECT term_id FROM osha_term_taxonomy WHERE term_taxonomy_id = " . $id . "";
    $datum = $wpdb->get_results($query);
    $id = $datum[0]->term_id;
}

if (isset($_POST['editcourse_type'])) {

    extract($_POST);
    //echo "<pre>";print_r($_POST);die;
    $title = wp_strip_all_tags($title);
    if ($_POST['change_status'] != "") {
        $status = $wpdb->get_results("SELECT term_status FROM osha_terms WHERE term_id = " . $id . "");
        if ($status[0]->term_status != $_POST['change_status']) {
            if ($_POST['change_status'] == 1) {
                $status = "inherit";
            } else {
                $status = "publish";
            }


            if ($wpdb->query('UPDATE osha_terms SET term_status = ' . $_POST['change_status'] . ' WHERE term_id=' . $id)) {

                $wpdb->query("UPDATE osha_posts JOIN osha_term_relationships ON osha_posts.ID=osha_term_relationships.object_id JOIN osha_term_taxonomy on osha_term_relationships.term_taxonomy_id=osha_term_taxonomy.term_taxonomy_id JOIN osha_terms on osha_term_taxonomy.term_id=osha_terms.term_id SET osha_posts.post_status='$status' where osha_term_taxonomy.term_id=$id");
            }
        }
    }

    //wp_update_term( $id, 'tribe_events_cat', array('name'=>$title,'description'=>$content));
    $wpdb->query("Update osha_term_taxonomy set description='$content' where term_id=$id AND taxonomy='tribe_events_cat'");

    $wpdb->query("Update osha_terms set name='$title' where term_id=$term_id");




    update_term_meta($id, 'valid_for_upto', $valid_for);
    update_term_meta($id, 'category_currency', $currency);
    update_term_meta($id, 'category_cost', $cost);
    update_term_meta($id, 'course_outline', $course_outline);
    update_term_meta($id, 'course_learning_outcome', $learning_outcome);
    if (!empty($_FILES)) {
        foreach ($_FILES as $file) {
            if (is_array($file)) {
                upload_user_file($file, $id);
            }
        }
    }

    wp_redirect(SITE_URL.'/courses?edited=1');
    exit;
}

if ($_GET['id']) {

    $id = $_GET['id'];
    $query = "SELECT term_id FROM osha_term_taxonomy WHERE term_taxonomy_id = " . $id . "";
    $datum = $wpdb->get_results($query);
    $id = $datum[0]->term_id;

    $result = get_term_by('id', (int) $id, 'tribe_events_cat');
    $term_result = get_term_meta($id);

    //print_r($term_result);
    $valid_for = $term_result['valid_for_upto'][0];
    $currency = $term_result['category_currency'][0];
    $cost = $term_result['category_cost'][0];
    $course_outline = $term_result['course_outline'][0];
    $learning_outcome = $term_result['course_learning_outcome'][0];
    $attatch_id = $term_result['category_image'][0];
    if ($attatch_id) {
        $image_data = wp_get_attachment_image_src($attatch_id);
        $image_src = $image_data[0];
    } else
        $image_src = '';
}
?>
<script>
    function back() {
        location.href = '<?php echo SITE_URL;?>/courses';
    }
</script>
<style>.validate{color:#F00;}</style>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1> Edit Course Type </h1>
</div>



<div class="card" id="profile-main">


    <div style="padding:0px" class="pm-body clearfix">
        <form class=""  id="edit_course_type" enctype="multipart/form-data" method="post" >



            <?php if (isset($_GET['added']) && $_GET['added'] == 'success') { ?>
                <div class="row">
                    <div class="col-sm-12">


                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Event Added Successfully.
                        </div>
                    </div></div>

            <?php } ?>


            <div class="row">
                <div class="col-sm-8">
                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-city m-r-5"></i> Basic Details</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view1">
 
                                <dl class="">
                                    <dt>Title</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name="title" id="title" class="form-control" type="text" required="true" value="<?php echo $result->name; ?>">
                                        </div>
                                    </dd>
                                </dl>
									
                                <dl class="">
                                    <dt>Description</dt>
									 
                                    <dd>
                                     <!--div class="html-editor"><?php echo $result->description; ?>Pravesh</div-->
                                        <textarea class="input-block-level" id="summernote" name="content" required="true"><?php echo $result->description; ?></textarea>
                                    </dd>

                                </dl>

                                <dl class="">
                                    <dt>Course Outline</dt>
                                    <dd>
                                        <!--<div class="html-editor"></div>-->
                                        <textarea class="input-block-level" id="summernote_course_outline" name="course_outline" required="true"><?php echo $course_outline; ?></textarea>
                                    </dd>

                                </dl>
                                <dl class="">
                                    <dt>Learning Outcome</dt>
                                    <dd>
                                        <!--<div class="html-editor"></div>-->
                                        <textarea class="input-block-level" id="summernote_learning_outcome" name="learning_outcome" required="true"><?php echo $learning_outcome; ?></textarea>
                                    </dd>
                                </dl>

                            </div>
                        </div>
                    </div>







                </div>
                <div class="col-sm-4">

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-settings m-r-5"></i> Settings</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="">
                                    <dt>Valid For</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select  class="form-control" name="valid_for" id="valid_for">
                                                <option value="" >--Select Duration--</option>
                                                <option value="60" <?php if ($valid_for == 60) echo 'selected=selected'; ?>>5 Years</option>
                                                <option value="48" <?php if ($valid_for == 48) echo 'selected=selected'; ?>>4 Years</option>
                                                <option value="36" <?php if ($valid_for == 36) echo 'selected=selected'; ?>>3 Years</option>
                                                <option value="24" <?php if ($valid_for == 24) echo 'selected=selected'; ?>>2 Years</option>
                                                <option value="12" <?php if ($valid_for == 12) echo 'selected=selected'; ?>>1 Years</option>
                                                <option value="6" <?php if ($valid_for == 6) echo 'selected=selected'; ?>>6 Months</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>


                                <div class="row">
                                    <div class="col-sm-3">
                                        <dl class="">
                                            <dt>Currency</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="currency" id="currency" class="form-control" type="text" value="<?php if (isset($currency))
                echo $currency;
            else
                echo '$';
            ?>" readonly="readonly">
                                                </div>
                                            </dd>
                                        </dl>

                                    </div>
                                    <div class="col-sm-9">
                                        <dl class="">
                                            <dt>Cost</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="cost" id="cost" class="form-control" type="text" value="<?php echo $cost; ?>">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>

                                </div>





                                <dl class="">
                                    <dt>Featured Image</dt>
                                    <dd>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <span class="btn btn-primary btn-file m-r-10">
                                                <span class="fileinput-new"><?php if ($image_src) { ?> Change Image <?php } else { ?> Select Image<?php } ?></span>
                                                <span class="fileinput-exists">Change</span>
                                                <input type="file" name="file" onchange="readURL(this);" >
                                            </span>
                                            <span class="fileinput-filename"></span>
                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                        </div>
                                        <div id="image_preview">
                                            <?php if ($image_src) { ?>
                                                <p><strong>Preview</strong></p>
                                                <img id="category_image" src="<?php echo $image_src; ?>">
<?php } else { ?>
                                                <img id="category_image" src="<?php bloginfo('template_url'); ?>/no-image.jpg" height="100px" width="100px">
<?php } ?>

                                        </div>
                                    </dd>



                                </dl>



                                <dt></dt>


                                <dl class="">
                                    <dt>Change Status</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select  class="form-control" name="change_status" id="change_status">
                                                <option value="" >--Select Status--</option>
                                                <option value="2" <?php if ($result->term_status == 2) echo 'selected=selected'; ?>>Active</option>
                                                <option value="1" <?php if ($result->term_status == 1) echo 'selected=selected'; ?>>Inactive</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>

                            </div>



                        </div>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0 8% 30px;">
                    <button  class="btn btn-success waves-effect  pull-right" type="submit" name="editcourse_type">Save Changes</button>
                    <input type="hidden" name="term_id" value="<?php echo $id; ?>" />


                </div>
            </div>

    </div>
</form>
</div>
</div>


<?php
getfooter();
?>
<!-- Data Table -->
<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#image_preview').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category_image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(function () {
        $("#edit_course_type").validate({
            rules: {
                title: {required: true},
                /*cost: {required:true,digits:true}*/
            },
            submitHandler: function (form) {



                form.submit();

            }

        });
    });
</script>
