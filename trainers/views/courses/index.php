<?php
getheader();

if ($_GET['id'] || $_GET['pid']):

    include("course.php");

else:

    if (isset($_POST['addcourse_type'])) {

        extract($_POST);
        require_once("../../../wp-load.php");
        $title = wp_strip_all_tags($title);
        $term = term_exists($title, 'tribe_events_cat');
        if ($term !== 0 && $term !== null) {
            $response['status'] = false;
            $response['message'] = $title . ' Course Type Already Exists';
        } else {
            $status = wp_insert_term(wp_strip_all_tags($title), 'tribe_events_cat', array('description' => $content));
            if (isset($status['term_id']) && isset($status['term_taxonomy_id'])) {
                update_term_meta($status['term_id'], 'valid_for_upto', $valid_for);
                update_term_meta($status['term_id'], 'category_currency', $currency);
                update_term_meta($status['term_id'], 'category_cost', $cost);
                update_term_meta($status['term_id'], 'course_outline', $course_outline);
				update_term_meta($status['term_id'], 'course_learning_outcome', $learning_outcome);
			    if (!empty($_FILES)) {
                    foreach ($_FILES as $file) {
                        if (is_array($file)) {
                            upload_user_file($file, $status['term_id']);
                            $response['status'] = true;
                            $response['message'] = 'Course Type addedd Successfully ';
                        }
                    }
                } else {
                    update_term_meta($status['term_id'], 'category_image', '');
                    $response['status'] = true;
                    $response['message'] = 'Course Type addedd Successfully';
                }
            } else {
                $response['status'] = false;
                $response['message'] = "Can't Add Course Type.Please Try after Some time";
            }
        }
    }
    ?>
    <style>.validate{color:#F00;}</style>
    <div class="block-header">

        <div class="row">
            <div class="col-sm-6">
                <h1>Course Types</h1>
            </div>
            <!--        <div class="col-sm-6">
                            <a data-toggle="modal" href="#modalWider" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Course Type</a>
                    </div>-->
        </div>

    <?php if (isset($response['status']) && $response['message']) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissible" role="alert" <?php if ($response['status'] == false) echo "style=color:red;"; ?>>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <?php echo $response['message']; ?>
                    </div>
                </div>
            </div>
    <?php } ?>


        <?php if (isset($_GET['edited']) && $_GET['edited'] == 1) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        Course Type Edited Successfully
                    </div>
                </div>
            </div>
        <?php } ?>



    </div>



    <div class="modal fade" id="modalWider" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Course Type</h4>
                </div>
                <form class="form-signin form-horizontal" id="add_course_type" enctype="multipart/form-data" method="post">
                    <div class="modal-body">
                        <div style = "display:none" class="err alert alert-danger" id="add_err"></div>
                        <div class="card-body card-padding">
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-city m-r-5"></i> Basic Details</h2>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">

                                                <dl class="">
                                                    <dt>Course Type Name</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input name="title" id="title" class="form-control" type="text" required="true" >
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <dl class="">
                                                    <dt>Upload Image</dt>
                                                    <dd>
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <span class="btn btn-primary btn-file m-r-10">
                                                                <span class="fileinput-new">Select file</span>
                                                                <span class="fileinput-exists">Change</span>
                                                                <input type="file" name="file" onchange="readURL(this);" >
                                                            </span>
                                                            <span class="fileinput-filename"></span>
                                                            <a href="#" class="close fileinput-exists" data-dismiss="fileinput">&times;</a>
                                                        </div>
                                                        <div id="image_preview" style="display:none;">
                                                            <p><strong>Preview</strong></p>
                                                            <img id="category_image" src="">
                                                        </div>
                                                    </dd>
                                                </dl>
                                                <dl class="">
                                                    <dt>Description</dt>
                                                    <dd>
                                                        <!--<div class="html-editor"></div>-->
                                                        <textarea class="input-block-level" id="summernote" name="content" required="true"></textarea>
                                                    </dd>
                                                </dl>
                                                <dl class="">
                                                    <dt>Course Outline</dt>
                                                    <dd>
                                                        <!--<div class="html-editor"></div>-->
                                                        <textarea class="input-block-level" id="summernote_course_outline" name="course_outline" required="true"></textarea>
                                                    </dd>
                                                </dl>
                                                
                                                <dl class="">
                                                    <dt>Learning Outcome</dt>
                                                    <dd>
                                                        <!--<div class="html-editor"></div>-->
                                                        <textarea class="input-block-level" id="summernote_learning_outcome" name="learning_outcome" required="true"></textarea>
                                                    </dd>
                                                </dl>



                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="pmb-block">
                                        <div class="pmbb-header">
                                            <h2><i class="zmdi zmdi-settings m-r-5"></i> Settings</h2>
                                        </div>
                                        <div class="pmbb-body p-l-30">
                                            <div class="pmbb-view">
                                                <dl class="">
                                                    <dt>Valid for upto</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <select placeholder="Course Instructor..." class="form-control" name="valid_for" id="valid_for">
                                                                <option value="60">5 Years</option>
                                                                <option value="48">4 Years</option>
                                                                <option value="36">3 Years</option>
                                                                <option value="24">2 Years</option>
                                                                <option value="12">1 Years</option>
                                                                <option value="6">6 Months</option>
                                                            </select>
                                                        </div>
                                                    </dd>
                                                </dl>

                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <dl class="">
                                                            <dt>Currency</dt>
                                                            <dd>
                                                                <div class="fg-line">
                                                                    <input name="currency" id="currency" class="form-control" type="text" value="$" readonly="readonly">
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                    <div class="col-sm-9">
                                                        <dl class="">
                                                            <dt>Cost</dt>
                                                            <dd>
                                                                <div class="fg-line">
                                                                    <input name="cost" id="cost" class="form-control" type="text">
                                                                </div>
                                                            </dd>
                                                        </dl>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button  id="addcourse_type" name="addcourse_type" type="submit" class="btn btn-link">Add Course Type</button>
                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php //echo course_categories();  ?>

    <div class="card">
        <div class="card-header">

            <div class="row">
                <div class="col-sm-6">
                    <h2>Course Types</h2>
                </div>
                <div class="col-sm-6">
                    <a data-toggle="modal" href="#modalWider" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Course Type</a>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="public_course_table" class="table table-striped table-vmiddle">
                <thead>
                    <tr>
                        <th data-column-id="id" data-type="numeric" data-order="desc">Course Code</th>
                        <th data-column-id="name">Course Type</th>
                        <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                    </tr>
                </thead>
                <tbody>
    			<?php echo courseTypes(); ?>
                </tbody>
            </table>
        </div>
    </div>



    <?php
    getfooter();

endif;
?>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#image_preview').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category_image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(function () {
        $("#add_course_type").validate({
            rules: {
                title: {required: true},
                cost: {required: true, digits: true}
            },
            submitHandler: function (form) {

                form.submit();

            }

        });


        $("#public_course_table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {

                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/category?id=" + row.id + "'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                    html += "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/editcoursetype?id=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button>";

                    //html +=	"<button type=\"button\" class=\"btn bgm-gray btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"  data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";

                    return html;
                }
            }
        });

    });
</script>
