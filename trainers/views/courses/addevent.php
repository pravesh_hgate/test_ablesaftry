<?php
getheader();
?>
<?php
$pageTitle = 'Add Public Class';

if ($_GET['id']) {
    $id = $_GET['id'];
}
$instructorId = "";
if ($_GET['objid']) {
    $obejId = $_GET['objid'];
    $pageTitle = 'Edit Public Class';
    //$STH = $asdb->query('SELECT osha_terms.term_id,name FROM osha_terms INNER JOIN osha_term_taxonomy ON osha_terms.term_id = osha_term_taxonomy.term_id WHERE osha_term_taxonomy.term_taxonomy_id =' . $id);
    $private = 0;

    $STH = $asdb->prepare("SELECT term_taxonomy_id
								FROM osha_term_relationships
								WHERE object_id = ?");

    $STH->execute(array($obejId));

    $cat = $STH->fetch(PDO::FETCH_ASSOC)['term_taxonomy_id'];

    $STH = $asdb->prepare('SELECT post_title, osha_postmeta.meta_value, osha_postmeta.meta_key
							FROM osha_posts
							INNER JOIN osha_postmeta ON osha_posts.ID = osha_postmeta.post_id
							WHERE osha_posts.ID = ?');

    $STH->execute(array($obejId));
    $course = $STH->fetchAll();

    foreach ($course as $key => $row) {
        switch ($row['meta_key']) {
            case "dateAndTime":
                $dateAndTime = unserialize($row['meta_value']);
                break;
            case "_EventVenueID":
                $venueID = $row['meta_value'];
                break;
            case "_EventCost":
                $cost = $row['meta_value'];
                break;
            case "seatNum":
                $seatNum = $row['meta_value'];
                break;
            case "_EventURL":
                $siteUrl = $row['meta_value'];
                break;
        }
    }

    $STH = $asdb->prepare('SELECT osha_postmeta.meta_value, osha_postmeta.meta_key
							FROM osha_posts
							INNER JOIN osha_postmeta ON osha_posts.ID = osha_postmeta.post_id
							WHERE osha_posts.ID = ?');

    $STH->execute(array($venueID));
    $venue = $STH->fetchAll();

    foreach ($venue as $key => $row) {
        switch ($row['meta_key']) {
            case "_VenueVenue":
                $venue_name = $row['meta_value'];
                break;
            case "_VenueAddress":
                $venue_address = $row['meta_value'];
                break;
            case "_VenueCity":
                $venue_city = $row['meta_value'];
                break;
            case "_VenueProvince":
                $venue_province = $row['meta_value'];
                break;
            case "_VenueCountry":
                $venue_country = $row['meta_value'];
                break;
            case "_VenueZip":
                $venue_zip = $row['meta_value'];
                break;
        }
    }
    $STH = $asdb->prepare('SELECT trainer_id FROM course_instructor where course_id = ?');
    $STH->execute(array($obejId));
    $trainerData = $STH->fetchAll();
    $instructorId = $trainerData[0]['trainer_id'];
}
?>
<script>
    function back() {
        location.href = '<?php echo SITE_URL;?>/courses/category?id=<?php echo $id; ?>';
    }
</script>
<style>.validate{color:#F00;}</style>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1><?php echo $pageTitle; ?></h1>
</div>



<div class="card" id="profile-main">


    <div style="padding:0px" class="pm-body clearfix">
        <form class="" id="add-public-class" method="post" >
            <div class="row" id="ajax_loader" style="display:none;">
                <div class="col-sm-12">
                    <img src="<?php echo URL_INCLUDE; ?>/img/ajax-loader.gif" alt=""> Please Wait...
                </div>
            </div>

            <div class="row" id="ajax_loader_div" style="display:none;">
                <div class="col-sm-12" id="ajax_response" style="color:#F00;">

                </div>
            </div>

            <?php if (isset($_GET['added']) && $_GET['added'] == 'success') { ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            Event Added Successfully.
                        </div>
                    </div>
                </div>

            <?php } ?>


            <div class="row">
                <div class="col-sm-8">
                    <!--                 <div class="pmb-block">
                                    <div class="pmbb-header">
                                        <h2><i class="zmdi zmdi-city m-r-5"></i> Basic Details</h2>
                                    </div>
                                </div>-->

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-google-maps m-r-5"></i> Venue Details</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">

                                <dl class="">
                                    <dt>Venue Name</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <?php
                                            if (!empty($venueID)) {
                                                $q = $asdb->query("SELECT `post_title`, post_status FROM `osha_posts` WHERE ID=" . $venueID . "");
                                                $f = $q->fetch();
                                                $venue_name = $f['post_title'];
                                            }
                                            ?>
                                            <input name="venue_name" id="venue_name" class="form-control" type="text" value="<?php echo $venue_name; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="">
                                    <dt>Address</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name="venue_address" id="venue_address" class="form-control" type="text" value="<?php echo $venue_address; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>City</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="city" id="city" class="form-control" type="text" value="<?php echo $venue_city; ?>">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>State or Province</dt>
                                            <dd>
                                                <div class="fg-line">
                                                <!--<input name="state" id="state" class="form-control" type="text">-->
                                                    <select name="state" id="state" class="form-control">
                                                        <?php
                                                        foreach ($us_states_abbrev as $k => $v) {
                                                            ?>
                                                            <option <?php if ($venue_province == $k) echo ' selected="selected"'; ?> value="<?php echo $k; ?>"><?php echo $v; ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Country</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select placeholder="Choose a Country..." class="form-control" name="country" id="country">
                                                        <option <?php if ($venue_country == "United States") echo ' selected="selected"'; ?> value="United States">United States</option>
                                                        <option <?php if ($venue_country == "United Kingdom") echo ' selected="selected"'; ?> value="United Kingdom">United Kingdom</option>
                                                        <option <?php if ($venue_country == "Afghanistan") echo ' selected="selected"'; ?> value="Afghanistan">Afghanistan</option>
                                                        <option <?php if ($venue_country == "Aland Islands") echo ' selected="selected"'; ?> value="Aland Islands">Aland Islands</option>
                                                        <option <?php if ($venue_country == "Albania") echo ' selected="selected"'; ?> value="Albania">Albania</option>
                                                        <option <?php if ($venue_country == "Algeria") echo ' selected="selected"'; ?> value="Algeria">Algeria</option>
                                                        <option <?php if ($venue_country == "American Samoa") echo ' selected="selected"'; ?> value="American Samoa">American Samoa</option>
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Postal Code</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="zip" id="zip" class="form-control" type="text" value="<?php echo $venue_zip; ?>">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-calendar m-r-5"></i> Schedule Dates</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Date</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control date-picker" id="schedule_date" name="schedule_date" placeholder="Click here..."  >
                                                </div>
                                            </dd>
                                        </dl>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <dl class="">
                                                    <dt>From</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control time-picker" id="startTime" name="startTime"  placeholder="Click here...">
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div class="col-sm-6">
                                                <dl class="">
                                                    <dt>To</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input type="text" class="form-control time-picker" id="endTime" name="endTime"  placeholder="Click here...">
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <a id = "addDate" class="btn btn-primary btn-sm waves-effect">Add</a>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Schedule List</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class="from-control pclass" name="dateList[]" id="dateList" size="5" style="height:100px; width: 100%;" multiple="">
                                                        <?php
                                                        if (!empty($dateAndTime[0])) {
                                                            foreach ($dateAndTime as $key => $value) {
                                                                ?>
                                                                <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                        <a id="removeFromList" class="col-sm-5 btn btn-primary btn-sm waves-effect">Remove</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-city m-r-5"></i> Coupon Details</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <?php
                                $couponData = array();
                                if (!empty($obejId)) {
                                    $q = $asdb->query("SELECT code, amount, coupon_type, start_date, expire_on FROM `coupons` where course_id=" . $obejId);
                                    $couponData = $q->fetch();
                                }
                                ?>
                                <dl class="">
                                    <dt>Coupon Code</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name="coupon_code" id="coupon_code" class="form-control" value="<?php echo $couponData['code']; ?>" type="text" <?php
                                            if (!empty($obejId)) {
                                                echo "readonly='true'";
                                            }
                                            ?>>
                                        </div>
                                    </dd>
                                </dl>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Start Date</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control date-picker" id="coupon_start" name="coupon_start" value="<?php echo ($couponData['start_date'] ? date("d/m/Y", strtotime($couponData['start_date'])) : ''); ?>" placeholder="Coupon Start Date..."  <?php
                                                    if (!empty($obejId)) {
                                                        echo "readonly='true'";
                                                    }
                                                    ?>>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>End Date</dt>

                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control date-picker" id="coupon_end" value="<?php echo ($couponData['expire_on'] ? date("d/m/Y", strtotime($couponData['expire_on'])) : ''); ?>" name="coupon_end" placeholder="Coupon Expire On..."  <?php
                                                    if (!empty($obejId)) {
                                                        echo "readonly='true'";
                                                    }
                                                    ?>>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <dl class="">
                                                    <dt>Currency</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input name="coupon_currency" id="coupon_currency" class="form-control" type="text" value="$" readonly="readonly">
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div class="col-sm-9">
                                                <dl class="">
                                                    <dt>Cost</dt>
                                                    <dd>
                                                        <div class="fg-line">
                                                            <input name="coupon_amount" id="coupon_amount" value="<?php echo $couponData['amount']; ?>" class="form-control" type="text" <?php
                                                            if (!empty($obejId)) {
                                                                echo "readonly='true'";
                                                            }
                                                            ?>>
                                                        </div>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <dl class="">
                                            <dt>Type</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select placeholder="Choose a Type..." class="form-control" name="coupon_type" id="coupon_type"  <?php
                                                    if (!empty($obejId)) {
                                                        echo "disabled='true'";
                                                    }
                                                    ?>>
                                                        <option value="1" <?php $couponData['coupon_type'] == 1 ? 'selected="selected"' : '' ?>>Fix Amount</option>
                                                        <option value="2" <?php $couponData['coupon_type'] == 2 ? 'selected="selected"' : '' ?>>Amount in Percentage</option>
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">

                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-settings m-r-5"></i> Settings</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="">
                                    <dt>No of Seats</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name="no_of_seats" id="no_of_seats" class="form-control" type="text" value="<?php echo $seatNum; ?>">
                                        </div>
                                    </dd>
                                </dl>

                                <dl class="">
                                    <dt>Instructor</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select placeholder="Course Instructor..." class="form-control" name="trainer" id="trainer">
                                                <?php
                                                $smt = $asdb->prepare("select trainer_id,first_name,last_name from trainers
                                         where first_name!='' AND last_name !=''");
                                                $smt->execute();
                                                $data = $smt->fetchAll();
                                                foreach ($data as $row) {
                                                    ?>
                                                    <option <?php if ($instructorId == $row['trainer_id']) echo ' selected="selected"'; ?> value="<?php echo $row['trainer_id']; ?>"><?php echo $row["first_name"] . $row["last_name"]; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>

                                <dl class="">
                                    <dt>Web Site</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name="website" id="website" class="form-control" type="text" placeholder="example.com" value="<?php echo $siteUrl; ?>">
                                        </div>
                                    </dd>
                                </dl>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <dl class="">
                                            <dt>Currency</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="currency" id="currency" class="form-control" type="text" value="$" readonly="readonly">
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="col-sm-9">
                                        <dl class="">
                                            <dt>Cost</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input name="cost" id="cost" class="form-control" type="text" value="<?php echo $cost; ?>" />
                                                </div>
                                            </dd>
                                        </dl>
                                    </div>
                                </div>
                                <dl class="">
                                    <dt>Post Status</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select name="post_status" class="form-control">
                                                <option value="publish" <?php if ($f['post_status'] == "publish") echo ' selected="selected"'; ?>>Publish</option>
                                                <option value="inherit" <?php if ($f['post_status'] == "inherit") echo ' selected="selected"'; ?>>Unpublish</option>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="hidden" name="category_id" value="<?php echo $id; ?>" />
                                        <input type="hidden" name="first_startdate" id="first_startdate" value="" />
                                        <input type="hidden" name="first_starttime" id="first_starttime" value="" />
                                        <input type="hidden" name="last_startdate" id="last_startdate" value="" />
                                        <input type="hidden" name="objId" value="<?php echo $obejId; ?>" />
                                    </div>
                                    <div class="col-sm-6">
                                        <!--<button  class="btn btn-primary  waves-effect">Reset</button>-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12" style="padding:0 8% 30px;"><button class="btn btn-primary btn-sm waves-effect pull-right" type="submit" name="schedule_class">Save</button></div>
            </div>
        </form>
    </div>
</div>


<?php
getfooter();
?>
<!-- Data Table -->
<script type="text/javascript">

    $(document).ready(function () {
        /*if ($('#startTime, #endTime, #schedule_date').val().length > 0) {
         $('a[id="addDate"]').attr("disabled", "false");

         }
         else {
         $('a[id="addDate"]').attr("disabled", "false");
         }*/

<?php if (empty($obejId)) { ?>
            $("#add-public-class").validate({
                rules: {
                    /*title: {required:true},*/
                    venue_address: {required: true},
                    city: {required: true},
                    state: {required: true},
                    zip: {required: true},
                    no_of_seats: {required: true, digits: true},
                    cost: {required: true},
                    summernote: {required: true},
                    coupon_cost: {digits: true},
                    coupon_amount: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    },
                    coupon_currency: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    },
                    coupon_start: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    },
                    coupon_end: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    }
                },
                submitHandler: function (form) {
                    var listElement = document.getElementById("dateList");
                    for (var i = 0; i < listElement.options.length; i++)
                    {
                        listElement.options[i].selected = true;
                    }

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo URL_PHP; ?>/courses/add_public_class.php",
                        data: $('#add-public-class').serialize(),
                        beforeSend: function () {
                            $('#schedule_class').prop("disabled", true);
                            $('#ajax_loader').show();
                        },
                        success: function (response) {
                            if (response.status == true)
                            {
                                location.href = '<?php echo SITE_URL;?>/courses/category?id=<?php echo $id; ?>';
                            } else
                            {
                                $('#ajax_loader_div').show();
                                $('#ajax_loader').hide();
                                $('#ajax_response').html(response.message);

                            }
                        },
                        complete: function (data) {
                            $('#schedule_class').prop("disabled", false);
                            $('#ajax_loader').hide();

                        }
                    });

                }
            });
<?php } else { ?>
            $("#add-public-class").validate({
                rules: {
                    venue_name: {required: true},
                    /*title: {required:true},*/
                    venue_address: {required: true},
                    city: {required: true},
                    state: {required: true},
                    zip: {required: true},
                    no_of_seats: {required: true, digits: true},
                    cost: {required: true},
                    summernote: {required: true},
                    coupon_amount: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        },
                    },
                    coupon_currency: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    },
                    coupon_start: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    },
                    coupon_end: {
                        required: function (element) {
                            return $("#coupon_code").val().length > 0;
                        }
                    }


                },
                submitHandler: function (form) {
                    var listElement = document.getElementById("dateList");
                    for (var i = 0; i < listElement.options.length; i++)
                    {
                        listElement.options[i].selected = true;
                    }

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "<?php echo URL_PHP; ?>/courses/add_public_class.php",
                        data: $('#add-public-class').serialize(),
                        beforeSend: function () {
                            $('#schedule_class').prop("disabled", true);
                            $('#ajax_loader').show();
                        },
                        success: function (response) {
                            if (response.status == true)
                            {
                                location.href = '<?php echo SITE_URL;?>/courses/category?id=<?php echo $id; ?>';
                            } else
                            {
                                $('#ajax_loader_div').show();
                                $('#ajax_loader').hide();
                                $('#ajax_response').html(response.message);

                            }
                        },
                        complete: function (data) {
                            $('#schedule_class').prop("disabled", false);
                            $('#ajax_loader').hide();

                        }
                    });

                }
            });
<?php } ?>
    });

    var counter = 1;
    function addToList()
    {
        var listElement = document.getElementById("dateList");
        var option = document.createElement("option");
        var dateElement = document.getElementById("schedule_date");
        var startTime = document.getElementById("startTime");
        var endTime = document.getElementById("endTime");
        var dateElementArray = dateElement.value.split("/");

        if (counter == 1)
        {
            document.getElementById("first_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
            document.getElementById("first_starttime").value = startTime.value;
            document.getElementById("last_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
        } else
        {
            document.getElementById("last_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
        }

        var dateElementArray = dateElement.value.split("/");

        var fixedDateString = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];

        var dateobj = new Date(fixedDateString);
        var dateString = dateobj.toUTCString();
        var dateArray = dateString.split(" ");

        var dateTextForm = dateArray[2] + " " + dateArray[1] + ", " + dateArray[3] + " From: " + startTime.value +
                " To: " + endTime.value + "  ";

        option.text = dateTextForm;
        option.value = dateTextForm;

        listElement.add(option);
        counter++;

    }


    $('#addDate').click(function () {

        if ($('#startTime, #endTime, #schedule_date').val().length > 0) {
            addToList();
            return false;
        }
    });

    $('#removeFromList').click(function () {

        var listElement = document.getElementById("dateList");
        listElement.remove(listElement.selectedIndex);
    });


    function getValues(selector, element)
    {
        var tempValues = "";
        var listElement = document.getElementById("dateList");

        for (var i = 0; i < listElement.options.length; i++)
        {
            listElement.options[i].selected = true;
        }

        $(selector).each(function () {
            var th = $(this);
            tempValues += th.attr('name') + "=" + th.val() + "&";

        });

        tempValues += "button=" + element.id + "&cat=<?php echo $_GET['id']; ?>";
        return tempValues;
    }


</script>
