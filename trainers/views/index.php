<?php
echo "SSSSS";exit;

 if(isLoggedIn()) { header( 'Location: http://104.236.4.188/ablesafety/trainers/dashboard' ); }?>
<html>
<!-- <head>
        <title>Login</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="include/Bootstrap-3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="include/css/login.css"/>
        
        <script type = "text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type = "text/javascript" src="include/Bootstrap-3.3.5/js/bootstrap.min.js"></script>
        <script type = "text/javascript" src = "include/js/login.js"></script>
        
    </head>
        <body>
       
     <div class="container loginform-in">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4">
                    <h1 class="text-center login-title">Sign into Able Safety Trainer's Hub</h1>
                   
                    <div class="account-wall">
                          <div style = "" class="err alert alert-danger" id="add_err"></div>
                        <img class="profile-img" src="https://www.ablesafety.com/wp-content/uploads/2012/10/ablesafety.png" alt=""/>
                        <form class="form-signin" action="./" method="post">
                            <input size="30" type="text" id = "name" name = "name" class="form-control" placeholder="Username" required autofocus />
                            <input type="password" size="30"  name="word" id="word" class="form-control" placeholder="Password" required />
                            <input type="submit" id="login" name="login" value="Login" class="btn btn-lg btn-primary btn-block loginbutton" />
                            <label class="checkbox pull-left">
                                <input type="checkbox" value="remember-me">
                                Remember me
                            </label>
                        </form>
                    </div>
                </div>
            </div>
        </div> -->
<!--[if IE 9 ]><html class="ie9"><![endif]-->

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        
        <!-- Vendor CSS -->
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="<?php echo URL_INCLUDE; ?>/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
            
        <!-- CSS -->
        <link href="<?php echo URL_INCLUDE; ?>/css/app.min.1.css" rel="stylesheet">
        <link href="<?php echo URL_INCLUDE; ?>/css/app.min.2.css" rel="stylesheet">
    </head>
    <body class="login-content">
        <?php if(isset($_GET['invite'])){ ?>
            <!-- Register -->
        <div class="lc-block toggled" id="l-register">
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    <input name="rusername" type="text" class="form-control" placeholder="Username">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="remail" type="email" class="form-control"  value="<?php echo $_GET['invite']; ?>">
                </div>
            </div>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                <div class="fg-line">
                    <input name="first_name" type="text" class="form-control" placeholder="First Name">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                <div class="fg-line">
                    <input name="last_name" type="text" class="form-control" placeholder="Last Name">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-phone zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="phone" type="text" class="form-control" placeholder="Phone">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-city zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="address" type="text" class="form-control" placeholder="Address">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-city zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="city" type="text" class="form-control" placeholder="City">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-city zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="state" type="text" class="form-control" placeholder="State">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-city zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="zip" type="text" class="form-control" placeholder="Zip">
                </div>
            </div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-key zmdi-hc-fw"></i></span>
                <div class="fg-line">
                    <input name="rpwd" type="password" class="form-control" placeholder="Password">
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            
            <button onClick="submit(this)" id="reg" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
            
        </div>
        <?php } else { ?>
 <div class="lc-block toggled" id="l-login">
             <div style = "display:none;" class="err alert alert-danger" id="add_err"></div>
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                <div class="fg-line">
                    <input name="name" id="name" type="text" class="form-control" placeholder="Username">
                </div>
            </div>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                <div class="fg-line">
                    <input name="pwd" id="pwd" type="password" class="form-control" placeholder="Password">
                </div>
            </div>
            
            <div class="clearfix"></div>
            
            <div class="checkbox">
                <label>
                    <input type="checkbox" value="">
                    <i class="input-helper"></i>
                    Keep me signed in
                </label>
            </div>
            
            <button onClick="submit(this)" id="login" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
            
            <ul class="login-navigation">
                <li data-block="#l-forget-password" class="bgm-orange">Forgot Password?</li>
            </ul>
        </div>
        
        
        <!-- Forgot Password -->
        <div class="lc-block" id="l-forget-password">
            <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu risus. Curabitur commodo lorem fringilla enim feugiat commodo sed ac lacus.</p>
            
            <div class="input-group m-b-20">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line">
                    <input name="fpwd" type="text" class="form-control" placeholder="Email Address">
                </div>
            </div>
            
            <button onClick="submit(this)" id="fpwd" class="btn btn-login btn-danger btn-float"><i class="zmdi zmdi-arrow-forward"></i></button>
            
            <ul class="login-navigation">
                <li data-block="#l-login" class="bgm-green">Login</li>
            </ul>
        </div>
        <?php } ?>
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/Waves/dist/waves.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="<?php echo URL_INCLUDE; ?>/js/functions.js"></script>
        
    </body>
</html>
<script>

function submit(element) {
if($('#name').val()=='' || $('#pwd').val()=='')
{
	$("#add_err").css('display', 'inline', 'important');
    $("#add_err").html("Please enter username and password");
	return false;
}
    
var post_data = getValues(".form-control", element);

    $.ajax({
        url: "<?php echo URL_PHP; ?>/login.php",
        type: "POST",
        data:  post_data,
        success: function(html){
            if(html == "true")
            {
                window.location="/views/dashboard";
            } else if(html == "yes") {
                window.location="<?php echo SITE_URL;?>/";
            } else {

                $("#add_err").css('display', 'inline', 'important');
                $("#add_err").html("Wrong username or password");
            }
     
       }
    });
}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id;
  return tempValues;
}

</script>