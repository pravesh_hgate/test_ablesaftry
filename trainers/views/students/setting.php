<?php 
getheader(); 
$student_id = $_SESSION['user']['user_id'];

$STH = $asdb->prepare('SELECT * 
                    FROM user_list 
                    WHERE user_id = ?');

$STH->execute(array($student_id));

$student = $STH->fetch();

?>
<div class="block-header">
    <h2><?php echo $student['full_name']; ?></h2>

</div>

<div class="card" id="profile-main">
    

    <div class="clearfix">

        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-account m-r-5"></i> Change Password</h2>

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a data-pmb-action="edit" href="">Edit</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <dl class="dl-horizontal">
                        <dt>Password</dt>
                        <dd>*********</dd>
                    </dl>
                    
                </div>

                <div class="pmbb-edit">
                   <form class="form-signin" action="#" method="post" onSubmit="return false">
                   <div style = "display:none" class="err alert alert-danger" id="add_err"></div>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Old Password</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "opassword" id = "opassword" type="text" class="form-control" required>
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">New Password</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "npassword" id = "npassword" type="text" class="form-control" required>
                            </div>

                        </dd>
                    </dl>
                     <dl class="dl-horizontal">
                        <dt class="p-t-10">Confirm Password</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "cpassword" id = "cpassword" type="text" class="form-control" required>
                            </div>

                        </dd>
                    </dl>
                    <div class="m-t-30">
                        <button onclick = "changePassword(this)"  id = "cPassword" class="btn btn-primary btn-sm">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>


        
    </div>
</div>



<?php 

getfooter();

?>

<script>

function changePassword(element) {
if($('#opassword').val()=='' || $('#npassword').val()=='' || $('#cpassword').val()=='')
{
	return false;
}	
if($('#npassword').val()!=$('#cpassword').val())
{
	
	$("#add_err").css('display', '', 'important');
    $("#add_err").html('New password and Cofirm password does not match');
	return false;
}	
var post_data = getValues(".form-control", element);
 
    $.ajax({
        url: "<?php echo URL_PHP; ?>/students/change_password.php",
        type: "POST",
        data:  post_data,
        success: function(html){
		
            if(html == "true")
            {
                $('.toggled').removeClass('toggled');
                notify('Successfully Updated', 'success');
                
               
            }
            else
            {
                $("#add_err").css('display', '', 'important');
                $("#add_err").html(html);
            }
     
       }
    });


}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id+"&id=<?php echo $student_id; ?>";
  return tempValues;
}
 

</script>