<?php 

$student_id = $_SESSION['user']['user_id'];

$STH = $asdb->prepare('SELECT * 
                    FROM user_list 
                    WHERE user_id = ?');

$STH->execute(array($student_id));

$student = $STH->fetch();

?>
<div class="block-header">
    <h2><?php echo $student['full_name']; ?></h2>

</div>

<div class="card" id="profile-main">
    <div class="pm-overview c-overflow">
        <div class="pmo-pic">
            <div class="p-relative">
                <a href="" onclick="document.getElementById('pic_upload').click(); return false">
                    <img id = "profile-image" class="img-responsive" src="<?php echo @getimagesize(URL_INCLUDE.'/uploads/profile_pics/student-'.$student_id.'.img')?URL_INCLUDE.'/uploads/profile_pics/student-'.$student_id.'.img':URL_INCLUDE.'/img/no_image.png'; ?>" alt=""> 
                </a>

                <div class="dropdown pmop-message">
                    <a data-toggle="dropdown" href="" class="btn bgm-white btn-float z-depth-1">
                        <i class="zmdi zmdi-comment-text-alt"></i>
                    </a>

                    <div class="dropdown-menu">
                        <textarea placeholder="Write something..."></textarea>

                        <button class="btn bgm-green btn-icon"><i class="zmdi zmdi-mail-send"></i></button>
                    </div>
                </div>

                <a href="" onclick="document.getElementById('pic_upload').click(); return false" class="pmop-edit">
                    <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                </a>
            </div>
            
            <input type="file" id = "pic_upload" class = "hidden" name="profile_pic" />
 
            <div class="pmo-stat">
                <h2 class="m-0 c-white">1562</h2>
                Total Courses
            </div>
        </div>

    </div>

    <div class="pm-body clearfix">

        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-account m-r-5"></i> Basic Information</h2>

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a data-pmb-action="edit" href="">Edit</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <dl class="dl-horizontal">
                        <dt>Full Name</dt>
                        <dd><?php echo $student['full_name']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Address</dt>
                        <dd><?php echo $student['address']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>City</dt>
                        <dd><?php echo $student['city']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>State</dt>
                        <dd><?php echo $student['state']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Zip</dt>
                        <dd><?php echo $student['zip']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Company Name</dt>
                        <dd><?php echo $student['company_name']; ?></dd>
                    </dl>
                </div>

                <div class="pmbb-edit">
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Full Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "full_name" type="text" class="form-control" value="<?php echo $student['full_name']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "address" type="text" class="form-control" value="<?php echo $student['address']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">City</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "city" type="text" class="form-control" value="<?php echo $student['city']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">State</dt>
                        <dd>
                            <div class="fg-line">
                                <select name = "state" class="form-control">
                                    <?php 
                                    
                                        foreach($us_states_abbrev as $abbrev=>$state)
                                        {
                                            echo "<option value = '".$abbrev."'>".$abbrev."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Zip</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "zip" type="text" class="form-control" value="<?php echo $student['zip']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Company Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "company_name" type="text" class="form-control" value="<?php echo $student['company_name']; ?>">
                            </div>

                        </dd>
                    </dl>
                    <div class="m-t-30">
                        <button onclick = "submit(this)" id = "editBasic" class="btn btn-primary btn-sm">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-phone m-r-5"></i> Contact Information</h2>

                <ul class="actions">
                    <li class="dropdown">
                        <a href="" data-toggle="dropdown">
                            <i class="zmdi zmdi-more-vert"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right">
                            <li>
                                <a data-pmb-action="edit" href="">Edit</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="pmbb-body p-l-30">
                <div class="pmbb-view">
                    <dl class="dl-horizontal">
                        <dt>Phone</dt>
                        <dd><?php echo $student['phone']; ?></dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt>Email Address</dt>
                        <dd><?php echo $student['email']; ?></dd>
                    </dl>
                </div>

                <div class="pmbb-edit">
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Phone</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "phone" type="text" class="form-control" value="<?php echo $student['phone']; ?>">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Email Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "email" type="email" class="form-control" value="<?php echo $student['email']; ?>">
                            </div>
                        </dd>
                    </dl>
 
                    <div class="m-t-30">
                        <button onclick = "submit(this)" id = "editContact" class="btn btn-primary btn-sm">Save</button>
                        <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-header">
    <h2>Taken Courses:</h2>

</div>

<?php echo student_upcoming_courses($student_id);

getfooter();

?>

<script>

function submit(element) {
    
var post_data = getValues(".form-control", element);

    $.ajax({
        url: "<?php echo URL_PHP; ?>/students/profile-ajax.php",
        type: "POST",
        data:  post_data,
        success: function(html){
            if(html == "true")
            {
                $('.toggled').removeClass('toggled');
                notify('Successfully Updated', 'success');
                
                $(".block-header h2").html( $(".pmbb-edit .form-control:eq(0)").val());
                for( i = 0; i < 8 ; i++){
                     $(".pmbb-view dl dd:eq("+i+")").html( $(".pmbb-edit .form-control:eq("+i+")").val());
                   }
            }
            else
            {
                notify('Error updating', 'danger');
            }
     
       }
    });
}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id+"&id=<?php echo $student_id; ?>";
  return tempValues;
}
        
$(document).on('change', "#pic_upload", function(){ 
    
    var data = new FormData();
    jQuery.each(jQuery('#pic_upload')[0].files, function(i, file) {
        data.append('file-'+i, file);
    });
    
    data.append("student_id", '<?php echo $student_id; ?>');
    console.log(data);
    $.ajax({
        url: '<?php echo URL_PHP; ?>/students/pic-upload-ajax.php',
        type: 'POST',
        data: data,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(html)
        {
            if(html == "true")
            {
                notify('Successfully Updated', 'success');
                d = new Date();
                $("#profile-image").attr("src", '<?php echo URL_INCLUDE; ?>/uploads/profile_pics/student-<?php echo $student_id; ?>.img?'+d.getTime());
            }
            else if(html == "type")
            {
                notify('File Type Not Supported, Please use gif, jpeg, jpg, or png.', 'warning');
            }
            else if(html == "big")
            {
                notify('File is too big.', 'warning');
            }
            else
            {
                notify('Error updating', 'danger');
            }
        },
        error: function(html)
        {
            // Handle errors here
            notify('Error updating', 'danger');
            // STOP LOADING SPINNER
        }
        
    });
    $('input[type="file"]').val(null);
    
});

</script>