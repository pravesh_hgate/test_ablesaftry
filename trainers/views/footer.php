</div>
</section>
        </section>
                <footer id="footer">
            Copyright &copy; 2015 Able Safety LLC.
            
            <ul class="f-menu">
                <li><a href="">Home</a></li>
                <li><a href="">Dashboard</a></li>
                <li><a href="">Reports</a></li>
                <li><a href="">Support</a></li>
                <li><a href="">Contact</a></li>
            </ul>
        </footer>
        
        <!-- Older IE warning message -->
        <!--[if lt IE 9]>
            <div class="ie-warning">
                <h1 class="c-white">Warning!!</h1>
                <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
                <div class="iew-container">
                    <ul class="iew-download">
                        <li>
                            <a href="http://www.google.com/chrome/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/chrome.png" alt="">
                                <div>Chrome</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mozilla.org/en-US/firefox/new/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/firefox.png" alt="">
                                <div>Firefox</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://www.opera.com">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/opera.png" alt="">
                                <div>Opera</div>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.apple.com/safari/">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/safari.png" alt="">
                                <div>Safari</div>
                            </a>
                        </li>
                        <li>
                            <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                                <img src="<?php echo URL_INCLUDE; ?>/img/browsers/ie.png" alt="">
                                <div>IE (New)</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <p>Sorry for the inconvenience!</p>
            </div>   
        <![endif]-->
        
        <!-- Javascript Libraries -->
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/flot/jquery.flot.time.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/sparklines/jquery.sparkline.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/moment/min/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/summernote/dist/summernote.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script type="text/javascript" src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        

        <script type="text/javascript" src="<?php echo URL_JS; ?>/functions.js"></script>
        <script type="text/javascript" src="<?php echo URL_JS; ?>/custom_functions.js"></script>
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bootgrid/jquery.bootgrid.min.js"></script>
     
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
		<script src="<?php echo URL_INCLUDE; ?>/vendors/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
        <script src="<?php echo URL_INCLUDE; ?>/js/validate.js"></script>
        <script src="<?php echo URL_INCLUDE; ?>/vendors/bower_components/chosen/chosen.jquery.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {
			$('#summernote').summernote({
					height: "200px"
			});
			$('#summernote_course_outline').summernote({
					height: "200px"
			});
			$('#summernote_learning_outcome').summernote({
					height: "200px"
			});
		})
		</script>
        
    </body>
  </html>