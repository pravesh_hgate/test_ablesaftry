<div class="block-header">
    <button onclick="location.href='<?php echo SITE_URL;?>/instructors';" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1> Create New Instructor </h1>
</div>
<div class="card" id="profile-main">
    <div class="pm-overview c-overflow">
        <div class="pmo-pic">
            <div class="p-relative">
               
                   <a href="" onclick="document.getElementById('pic_upload').click(); return false">
                    <img id = "profile-image"  class="img-responsive" src="<?php echo URL_INCLUDE; ?>/img/no_image.png" alt=""> 
                </a>

                

             
                 <a href="" onclick="document.getElementById('pic_upload').click(); return false" class="pmop-edit">
                    <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                </a>
                <input type="file" id = "pic_upload" class = "hidden" name="profile_pic" />
            </div>


            
        </div>

    </div>

    <div class="pm-body clearfix">

        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-account m-r-5"></i> Basic Information</h2>

                
            </div>
            <div class="pmbb-body p-l-30">
                

                <div >
                   <dl class="dl-horizontal">
                        <dt class="p-t-10">Username</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "username" id="username" type="text" class="form-control" >
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">First Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "first_name" id="first_name" type="text" class="form-control">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Last Name</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "last_name" type="text" class="form-control">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "address" type="text" class="form-control">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">City</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "city" type="text" class="form-control">
                            </div>

                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">State</dt>
                        <dd>
                            <div class="fg-line">
                                <select name = "state" class="form-control">
                                    <?php 
                                    
                                        foreach($us_states_abbrev as $abbrev=>$state)
                                        {
                                            echo "<option value = '".$abbrev."'>".$abbrev."</option>";
                                        }
                                    ?>
                                </select>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Zip</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "zip" type="text" class="form-control" >
                            </div>

                        </dd>
                    </dl>
                    
                </div>
            </div>
        </div>


        <div class="pmb-block">
            <div class="pmbb-header">
                <h2><i class="zmdi zmdi-phone m-r-5"></i> Contact Information</h2>

                
            </div>
            <div class="pmbb-body p-l-30">
                

                <div>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Phone</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "phone" type="text" class="form-control">
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Email Address</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "email" id="email" type="email" class="form-control">
                            </div>
                        </dd>
                    </dl>
 
                    <div class="m-t-30">
                        <input name = "image" id="image" type="hidden" class="form-control" value="">
                        <button onclick = "submit(this)" id = "createNew" class="btn btn-primary btn-sm">Save</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php

getfooter();
$code= rand();
$type='instructor';
?>

<script>
function IsEmail(email) {
	  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  if(!regex.test(email)) {
		 return false;
	  }else{
		 return true;
	  }
	}
function submit(element) {
var username = $('#username').val();
var first_name = $('#first_name').val();
var email = $('#email').val();
   if(username== ''){
	notify('Please enter your Username.', 'warning');
	return false;
  }
  if(first_name== ''){
	notify('Please enter your firstname.', 'warning');
	return false;
  }
  if(email== ''){
	notify('Please enter your Email.', 'warning');
	 return false;
  }
  if(IsEmail(email)==false){
	  notify('Please enter a valid email address.', 'warning');
	  return false;
  }


var post_data = getValues(".form-control", element);
    $.ajax({
        url: "<?php echo URL_PHP; ?>/instructor/new.php",
        type: "POST",
        data:  post_data,
        success: function(html){
		  if(html == "true")
            {
                $('.toggled').removeClass('toggled');
                notify('Successfully Updated', 'success');
               // window.location.href="";
				location.href='<?php echo SITE_URL;?>/instructors';
                $(".block-header h2").html( $(".pmbb-edit .form-control:eq(0)").val());
                for( i = 0; i < 8 ; i++){
                     $(".pmbb-view dl dd:eq("+i+")").html( $(".pmbb-edit .form-control:eq("+i+")").val());
                   }
            }
            else
            {
                notify('Error updating', 'danger');
            }
     
       }
    });
}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id;
  return tempValues;
}


$(document).on('change', "#pic_upload", function(){ 
    var data = new FormData();
    jQuery.each(jQuery('#pic_upload')[0].files, function(i, file) {
        data.append('file-'+i, file);
    });
    data.append("type", '<?php echo $type; ?>');
    data.append("code", '<?php echo $code; ?>');
    console.log(data);
    $.ajax({
        url: '<?php echo URL_PHP; ?>/instructor/pic-upload-ajax.php',
        type: 'POST',
        data: data,
        processData: false, // Don't process the files
        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        success: function(html)
        {
            if(html == "true")
            {
                notify('Successfully Updated', 'success');
                d = new Date();
                $("#profile-image").attr("src", '<?php echo URL_INCLUDE; ?>/uploads/temp/<?php echo $type; ?>-<?php echo $code; ?>.img?'+d.getTime());
				$("#image").val('<?php echo $type; ?>-<?php echo $code; ?>.img');
            }
            else if(html == "type")
            {
                notify('File Type Not Supported, Please use gif, jpeg, jpg, or png.', 'warning');
            }
            else if(html == "big")
            {
                notify('File is too big.', 'warning');
            }
            else
            {
                notify('Error updating', 'danger');
            }
        },
        error: function(html)
        {
            // Handle errors here
            notify('Error updating', 'danger');
            // STOP LOADING SPINNER
        }
        
    });
    $('input[type="file"]').val(null);
    
});
</script>