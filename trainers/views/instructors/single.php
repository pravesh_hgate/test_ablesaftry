<?php
$instructor_id = $_GET['id'];

$STH = $asdb->prepare('SELECT *
                    FROM trainers
                    WHERE trainer_id = ?');

$STH->execute(array($instructor_id));

$instructor = $STH->fetch();
?>
<div class="block-header">
    <h2><?php echo $instructor['first_name'] . " " . $instructor['last_name']; ?></h2>

</div>

<div class="card" id="profile-main">
    <div class="pm-overview c-overflow">
        <div class="pmo-pic">
            <div class="p-relative">
                <a href="" onclick="document.getElementById('pic_upload').click(); return false">
                    <img id = "profile-image" class="img-responsive" src="<?php echo @getimagesize(URL_INCLUDE . '/uploads/instructor_profile_pic/instructor-' . $instructor_id . '.img') ? URL_INCLUDE . '/uploads/instructor_profile_pic/instructor-' . $instructor_id . '.img' : URL_INCLUDE . '/img/no_image.png'; ?>" alt="">
                </a>

                <div class="dropdown pmop-message">
                    <a data-toggle="dropdown" href="" class="btn bgm-white btn-float z-depth-1">
                        <i class="zmdi zmdi-comment-text-alt"></i>
                    </a>

                    <div class="dropdown-menu">
                        <textarea placeholder="Write something..."></textarea>

                        <button class="btn bgm-green btn-icon"><i class="zmdi zmdi-mail-send"></i></button>
                    </div>
                </div>

                <a href="" onclick="document.getElementById('pic_upload').click(); return false" class="pmop-edit">
                    <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                </a>
                <input type="file" id = "pic_upload" class = "hidden" name="profile_pic" />
            </div>


            <div class="pmo-stat">
                <h2 class="m-0 c-white">1562</h2>
                Total Courses
            </div>
        </div>
        <div class="pmo-block pmo-contact hidden-xs" style="">
            <h2>Other</h2>

            <ul>
                <li><div class="toggle-switch">
                        <label for="ts1" class="ts-label">Status</label>
                        <?php if ($instructor['user_status'] == 2) { ?>
                            <input id="ts1" type="checkbox" hidden="hidden" onchange="statusChange(this.val)" checked="checked">
                        <?php } else { ?>
                            <input id="ts1" type="checkbox" hidden="hidden" onchange="statusChange(this.val)">
                        <?php } ?>
                        <label for="ts1" class="ts-helper"></label>
                    </div>
                </li>
                <li><strong style="display: inline-block; width: 62%;">Generate Password:</strong> <a href="javascript:void(0);" id="<?php echo $instructor['trainer_id']; ?>" onclick="generatePasswd(this.id)">Generate</a></li>
                <li><strong style="display: inline-block; width: 62%;">Current Password:</strong> <span id="currentPassword"><?php echo $instructor['password']; ?></span></li>
                <li>
                    <!--<strong style="display: inline-block; width: 62%;">Hourly Rate</strong> <input type="text" name="hourly_rate" id="hourly_rate" />-->
                    <div class="input-group" style="width:100%;">
                        <div align="right" class="pull-right" style="width:100%">
                            <ul class="actions" style="float:right; clear:both;">
                                <li dropdown="" class="dropdown">
                                    <a data-toggle="dropdown" aria-expanded="false" href="javascript:void(0);">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="javascript:void(0);" id="editDollar" onclick="editDollarPrice();">Edit</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div align="right" class="pull-left" style="width:100%">
                            <div id="statDollor" style="float:left; margin: 4px 0 0 16px;">
                                Hourly Rate:
                                <span class="input-group-addon" style="position:relative;"><i class="zmdi zmdi-money" style="top:5px; font-size: 18px;"></i></span>
                                <?php echo $instructor['hourly_rate']; ?>
                            </div>
                            <div class="fg-line" id="dyndollar" style="display:none;">
                                <span class="input-group-addon"><i class="zmdi zmdi-money" style="top:5px; font-size: 18px; left: -10px;"></i></span>
                                <input type="text" placeholder="Hourly Rate" name="hourly_rate" id="hourly_rate" class="form-control" value="<?php echo $instructor['hourly_rate']; ?>" />
                                <button class="btn btn-primary btn-sm waves-effect" type="submit" onclick="hourlyRate();">Save</button>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="pm-body clearfix">
        <div role="tabpanel">
            <ul class="tab-nav" role="tablist">
                <li class="active"><a href="#home11" aria-controls="home11" role="tab" data-toggle="tab" style="padding: 15px 50px;">Basic Information</a></li>
                <li><a href="#profile11" aria-controls="profile11" role="tab" data-toggle="tab" style="padding: 15px 50px;">Billing Address</a></li>
                <li><a href="#messages11" aria-controls="messages11" role="tab" data-toggle="tab" style="padding: 15px 50px;">Office Address</a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="home11">
                    <div class="pmb-block" style="padding-top: 10px;">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-account m-r-5"></i>Basic Information</h2>

                            <ul class="actions">
                                <li class="dropdown">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a data-pmb-action="edit" href="">Edit</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>First Name</dt>
                                    <dd><?php echo $instructor['first_name']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Last Name</dt>
                                    <dd><?php echo $instructor['last_name']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Address</dt>
                                    <dd><?php echo $instructor['address']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>City</dt>
                                    <dd><?php echo $instructor['city']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>State</dt>
                                    <dd><?php echo $instructor['state']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Zip</dt>
                                    <dd><?php echo $instructor['zip']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Phone</dt>
                                    <dd><?php echo $instructor['phone'] ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Email Address</dt>
                                    <dd><?php echo $instructor['email']; ?></dd>
                                </dl>
                            </div>

                            <div class="pmbb-edit">
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">First Name</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "first_name" type="text" class="form-control" value="<?php echo $instructor['first_name']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Last Name</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "last_name" type="text" class="form-control" value="<?php echo $instructor['last_name']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Address</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "address" type="text" class="form-control" value="<?php echo $instructor['address']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">City</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "city" type="text" class="form-control" value="<?php echo $instructor['city']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">State</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select name = "state" class="form-control">
                                                <?php
                                                foreach ($us_states_abbrev as $abbrev => $state) {
                                                    echo "<option value = '" . $abbrev . "'>" . $abbrev . "</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Zip</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "zip" type="text" class="form-control" value="<?php echo $instructor['zip']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Phone</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "phone" type="text" class="form-control" value="<?php echo $instructor['phone']; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Email Address</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "email" type="email" class="form-control" value="<?php echo $instructor['email']; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <div class="m-t-30">
                                    <button onclick = "submit(this)" id = "editBasic" class="btn btn-primary btn-sm">Save</button>
                                    <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="profile11">
                    <div class="pmb-block" style="padding-top: 10px;">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-account m-r-5"></i>Billing Address</h2>

                            <ul class="actions">
                                <li class="dropdown">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a data-pmb-action="edit" href="">Edit</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Address</dt>
                                    <dd><?php echo $instructor['billing_address']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>City</dt>
                                    <dd><?php echo $instructor['billing_city']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>State</dt>
                                    <dd><?php echo $instructor['billing_state']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Zip</dt>
                                    <dd><?php echo $instructor['billing_zip']; ?></dd>
                                </dl>
                            </div>

                            <div class="pmbb-edit">
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Address</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "billing_address" type="text" class="form-control" value="<?php echo $instructor['billing_address']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">City</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "billing_city" type="text" class="form-control" value="<?php echo $instructor['billing_city']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">State</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select name = "billing_state" class="form-control">
                                                <?php
                                                foreach ($us_states_abbrev as $abbrev => $state) {
                                                    if ($instructor['billing_state'] === $abbrev) {
                                                        echo "<option value = '" . $abbrev . "' selected>" . $abbrev . "</option>";
                                                    } else {
                                                        echo "<option value = '" . $abbrev . "'>" . $abbrev . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Zip</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "billing_zip" type="text" class="form-control" value="<?php echo $instructor['billing_zip']; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <div class="m-t-30">
                                    <button onclick = "submit(this)" id = "editBilling" class="btn btn-primary btn-sm">Save</button>
                                    <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="messages11">
                    <div class="pmb-block" style="padding-top: 10px;">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-account m-r-5"></i>Office Address</h2>

                            <ul class="actions">
                                <li class="dropdown">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a data-pmb-action="edit" href="">Edit</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Address</dt>
                                    <dd><?php echo $instructor['office_address']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>City</dt>
                                    <dd><?php echo $instructor['office_city']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>State</dt>
                                    <dd><?php echo $instructor['office_state']; ?></dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Zip</dt>
                                    <dd><?php echo $instructor['office_zip']; ?></dd>
                                </dl>
                            </div>

                            <div class="pmbb-edit">
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Address</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "office_address" type="text" class="form-control" value="<?php echo $instructor['office_address']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">City</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "office_city" type="text" class="form-control" value="<?php echo $instructor['office_city']; ?>">
                                        </div>

                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">State</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <select name = "office_state" class="form-control">
                                                <?php
                                                foreach ($us_states_abbrev as $abbrev => $state) {
                                                    if ($instructor['office_state'] === $abbrev) {
                                                        echo "<option value = '" . $abbrev . "' selected>" . $abbrev . "</option>";
                                                    } else {
                                                        echo "<option value = '" . $abbrev . "'>" . $abbrev . "</option>";
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt class="p-t-10">Zip</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <input name = "office_zip" type="text" class="form-control" value="<?php echo $instructor['office_zip']; ?>">
                                        </div>
                                    </dd>
                                </dl>
                                <div class="m-t-30">
                                    <button onclick = "submit(this)" id = "editOffice" class="btn btn-primary btn-sm">Save</button>
                                    <button data-pmb-action="reset" class="btn btn-link btn-sm">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="card">
    <div class="card-header">
        <h2>Given Lectures :</h2>
    </div>
    <div class="table-responsive table-editable">
        <table id="data-table-selection" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="user_id" data-type="numeric" data-order="asc">Course Id</th>
                    <th data-formatter="editable" data-column-id="full_name">Course Name</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $STH = $asdb->query("SELECT osha_postmeta.meta_value, osha_posts.ID, osha_posts.post_title, course_instructor.trainer_id, trainers.first_name, trainers.last_name
                                    FROM osha_postmeta
                                    INNER JOIN osha_posts ON osha_posts.ID = osha_postmeta.post_id
                                    INNER JOIN orders ON orders.course_id = osha_posts.ID
									INNER JOIN course_instructor ON course_instructor.course_id = orders.course_id
									INNER JOIN trainers ON trainers.trainer_id = course_instructor.trainer_id
                                    WHERE trainers.trainer_id ='" . $instructor_id . "'
                                    AND osha_postmeta.meta_key = 'dateAndTime'
                                    ORDER BY osha_posts.ID DESC");

                while ($rows = $STH->fetch(PDO::FETCH_ASSOC)) {
                    ?>
                    <?php
                    $temp = unserialize($rows['meta_value']);

                    echo "<tr><td>" . $rows['ID'] . "</td>";
                    echo "<td><a href='" . SITE_URL . "/courses/?id=" . $rows['ID'] . "'>" . $rows['post_title'] . "</a></td></tr>";
                    ?>

                <?php } ?>


            </tbody>
        </table>
    </div>
</div>


<?php
getfooter();
$code = rand();
$type = 'instructor';
?>

<script>

    function submit(element) {

        var post_data = getValues(".form-control", element);

        $.ajax({
            url: "<?php echo URL_PHP; ?>/instructor/profile-ajax.php",
            type: "POST",
            data: post_data,
            success: function (html) {
                if (html == "true")
                {
                    $('.toggled').removeClass('toggled');
                    notify('Successfully Updated', 'success');

                    $(".block-header h2").html($(".pmbb-edit .form-control:eq(0)").val());
                    for (i = 0; i < 16; i++) {
                        $(".pmbb-view dl dd:eq(" + i + ")").html($(".pmbb-edit .form-control:eq(" + i + ")").val());
                    }
                } else
                {
                    notify('Error updating', 'danger');
                }

            }
        });
    }

    function generatePasswd(trainer) {
        $.ajax({
            url: "<?php echo URL_PHP; ?>/instructor/generate-password-ajax.php",
            type: "POST",
            data: {"trainer_id": trainer},
            success: function (html) {
                if (html != "false")
                {
                    $('#currentPassword').text(html);
                    notify('Password Generated successfully', 'success');
                } else
                {
                    notify('Problem in generating of password.', 'danger');
                }

            }
        });
    }

    function statusChange(val) {
        var value = jQuery('#ts1').prop('checked');
        var user_status = 1;
        if (value == true) {
            user_status = 2;
        }
        jQuery.ajax({
            url: "<?php echo URL_PHP; ?>/instructor/update_trainer_status.php",
            type: "POST",
            data: {"trainer_id": "<?php echo $instructor_id; ?>", 'user_status': user_status},
            success: function (html) {
                if (html != "false")
                {
                    notify('Status Updated successfully', 'success');
                } else
                {
                    notify('Problem in updating status.', 'danger');
                }

            }
        });
    }

    function editDollarPrice() {
        jQuery('#dyndollar').show();
        jQuery('#statDollor').hide();
    }
    function hourlyRate() {
        var val = jQuery('#hourly_rate').val();
        jQuery.ajax({
            url: "<?php echo URL_PHP; ?>/instructor/update_trainer_hourlyrate.php",
            type: "POST",
            data: {"trainer_id": "<?php echo $instructor_id; ?>", 'hourly_rate': val},
            success: function (html) {
                if (html != "false")
                {
                    notify('Hourly Rate Updated successfully', 'success');
                    window.location.reload();
                } else
                {
                    notify('Problem in updating hourly rate.', 'danger');
                }

            }
        });
    }


    function getValues(selector, element) {
        var tempValues = "";

        $(selector).each(function () {
            var th = $(this);
            tempValues += th.attr('name') + "=" + th.val() + "&";

        });
        tempValues += "button=" + element.id + "&id=<?php echo $instructor_id; ?>";
        return tempValues;
    }

    $(document).on('change', "#pic_upload", function () {
        var data = new FormData();
        jQuery.each(jQuery('#pic_upload')[0].files, function (i, file) {
            data.append('file-' + i, file);
        });
        data.append("target_dir", '<?php echo DIR_UPLOADS . "/instructor_profile_pic/"; ?>');
        data.append("type", '<?php echo $type; ?>');
        data.append("code", '<?php echo $instructor_id; ?>');
        console.log(data);
        $.ajax({
            url: '<?php echo URL_PHP; ?>/instructor/pic-upload-ajax.php',
            type: 'POST',
            data: data,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (html)
            {
                if (html == "true")
                {
                    notify('Successfully Updated', 'success');
                    d = new Date();
                    $("#profile-image").attr("src", '<?php echo URL_INCLUDE . '/uploads/instructor_profile_pic/instructor-' . $instructor_id . '.img' ?>?' + d.getTime());

                } else if (html == "type")
                {
                    notify('File Type Not Supported, Please use gif, jpeg, jpg, or png.', 'warning');
                } else if (html == "big")
                {
                    notify('File is too big.', 'warning');
                } else
                {
                    notify('Error updating', 'danger');
                }
            },
            error: function (html)
            {
                // Handle errors here
                notify('Error updating', 'danger');
                // STOP LOADING SPINNER
            }

        });
        $('input[type="file"]').val(null);

    });
</script>