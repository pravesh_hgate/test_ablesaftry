<?php
$STH = $asdb->query('SELECT * FROM trainers WHERE user_type=1');
if(isset($_POST['invite'])){
    echo 'hi';
}

?>
<script>
function assign(){
    location.href='<?php echo SITE_URL;?>/instructors/functions/?invite=true';
}
</script>
<!--<button class="btn btn-primary btn-lg waves-effect" onclick="showForm('#invitations'); return false;">Invite New Instructor</button>-->
</br>
<div class="card">
       <div class="card-header">
    	<div class="row">
            <div class = "col-sm-6">
            <h2>Your Instructors</h2>
            </div>
            <div class = "col-sm-6">
            	<a data-toggle="modal" href="<?php echo SITE_URL;?>/instructors/?create=new" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Instructor</a>
          		<!--<a class="btn btn-primary btn-block waves-effect" href="<?php echo SITE_URL;?>/courses/addstudent" style="width: 120px; float:right;">Add Student</a>--> 
            </div>
        </div>
    </div>                    
    <div class="table-responsive table-editable">
        <table id="data-table-selection" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="trainer_id" data-type="numeric" data-order="asc">ID</th>
                    <th data-formatter="editable" data-column-id="first_name">First Name</th>
                    <th data-formatter="editable" data-column-id="last_name">Last Name</th>
                    <th data-formatter="editable" data-column-id="email">Email Address</th>
                    <th data-formatter="editable" data-column-id='phone'>Phone #</th>
                    <th data-formatter="editable" data-column-id='city'>City</th>
                    <th data-formatter="editable" data-column-id='state'>State</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php  
            while($users = $STH->fetch(PDO::FETCH_ASSOC)){
        echo "<tr><td>".$users['trainer_id']."</td>";
        echo "<td contenteditable='true'>".$users['first_name']."</td>";
        echo "<td contenteditable='true'>".$users['last_name']."</td>";
        echo "<td contenteditable='true'>".$users['email']."</td>";
        echo "<td contenteditable='true'>".$users['phone']."</td>";
        echo "<td contenteditable='true'>".$users['city']."</td>";
        echo "<td contenteditable='true'>".$users['state']."</td></tr>";
    
                }
                ?>  
            </tbody>
        </table>
    </div>
</div>
<!--<div id="invitations" class="card">
                        <div class="card-header">
                            <h2>Send Instructor Invitation.</h2>
                        </div>
                        
                        <div class="card-body card-padding">
                                <dl class="dl-horizontal">
                        <dt class="p-t-10">Email Address:</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "email" type="text" class="form-control" placeholder="Email"/>
                            </div>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                        <dt class="p-t-10">Name:</dt>
                        <dd>
                            <div class="fg-line">
                                <input name = "name" type="email" class="form-control" placeholder="Full Name">
                            </div>
                        </dd>
                    </dl>
 
                    <div class="m-t-30">
                        <button onclick = "submit(this)" id = "invite" class="btn btn-primary btn-sm">Invite</button>
                    </div>
                        </div>
</div>-->


<?php getfooter(); ?>
<style>

    .edit{
        text-decoration: none;
        border: 0px;
        color: rgb(94,94,94);
    }
    #invitations{
        display: none;
    }  
</style>
<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function(){
        
        $.fn.editable.defaults.mode = 'inline';
        //Selection
        $("#data-table-selection").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            selection: true,
            multiSelect: true,
            rowSelect: true,
            keepSelection: true,
             formatters: {
                "editable": function(column, row) { 
                    
                    return '<a href="" style = "border-bottom: 0px;" class="edit" data-type="text" data-name="'+column.id+'" data-pk="'+row['user_id']+'" data-url="<?php echo URL_PHP; ?>/instructors/instructors.ajax.php" data-title="Enter Name">'+row[column.id]+'</a>';                    
                    
                },
                 "commands": function(column, row) {
				 	                   
                    var html= "<button onclick=\"location.href='<?php echo SITE_URL;?>/instructors/?id="+row.trainer_id+"'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.trainer_id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";
					html += " <button type=\"button\" class=\"btn btn-icon command-delete\" data-row-id=\"" + row.trainer_id + "\" onclick=\"deleteInstructor(this)\"><span class=\"zmdi zmdi-delete\"></span></button>";
					return html;
                }
                
             }
        }).on("loaded.rs.jquery.bootgrid", function (e){
              $('.edit').each(function(column, row){    
                $(this).editable(
                );
              })
             });

    });
    var curForm;
    function showForm(formId){
        if (curForm != null){ curForm.hide(); curForm = null;}
        else{
            curForm = $(formId);
            curForm.show();
        }
    }
    function submit(element) {
    
var post_data = getValues(".form-control", element);

    $.ajax({
        url: "<?php echo URL_PHP; ?>/instructor/invite.ajax.php",
        type: "POST",
        data:  post_data,
        success: function(html){
            if(html == "true")
            {
                notify('Invite Sent', 'success');
            } else {
                notify('Error Sending Invite', 'danger');
            }
     
       }
    });
}
    
function getValues(selector, element){
  var tempValues = "";
 
  $(selector).each(function(){
     var th= $(this);
     tempValues += th.attr('name')+"="+th.val()+"&";

   });
    tempValues += "button="+element.id;
  return tempValues;
}
function deleteInstructor(element) {
        
        var instructor_id = $(element).attr('data-row-id');
		 $.ajax({

           type : 'post',
           url : '<?php echo URL_PHP ?>/instructor/delete-instructor-ajax.php', 
           data :  {instructor_id: instructor_id},
           success : function(html)
           {
               if(html == "true")
               {
                 notify("Instructor has been deleted", "success");
				 location.reload();
               }
               else{
                notify("Instructor not deleted", "danger");
               }
           },
           error: function()
           {
               notify("There was an error.", "success");
           }
       });

    }
</script>