<?php
getheader();
?>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1>Order No : 111</h1>
</div>
<div class="card">
    <div class="card-header">

        <div class="row">
            <div class="col-sm-6">
                <h2>Order No : 111 <small>Order Date : Dec 05, 2016</small></h2>
            </div>
            <div class="col-sm-6">
                &nbsp;
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table id="order_detail" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id">Course Name</th>
                    <th data-column-id="totalstudents">Total No. Of Students</th>
                    <th data-column-id="unitPrice">Unit Price</th>
                    <th data-column-id="discount">Discount</th>
                    <th data-column-id="total">Total</th>
                </tr>
            </thead>
            <tbody>
                <tr data-row-id="0">
                    <td class="text-left">Test course type<br/>(12 Dec , 2016 - 13 Dec , 2016 )</td>
                    <td class="text-left">3</td>
                    <td class="text-left">$110</td>
                    <td class="text-left">$10</td>
                    <td class="text-left">$100</td>
                </tr>
                <tr data-row-id="1">
                    <td class="text-left">Test course type<br/>(15 Dec , 2016 - 17 Dec , 2016 )</td>
                    <td class="text-left">5</td>
                    <td class="text-left">$10</td>
                    <td class="text-left">$0</td>
                    <td class="text-left">$500</td>
                </tr>
                <tr data-row-id="2">
                	<td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left">Total</td>
                    <td class="text-left">$500</td>
                </tr>
                <tr data-row-id="3">
                	<td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left">Paid(05 Dec, 2016)</td>
                    <td class="text-left">$50</td>
                </tr>
                <tr data-row-id="3">
                	<td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left">Paid(06 Dec, 2016)</td>
                    <td class="text-left">$550</td>
                </tr>
                <tr data-row-id="4">
                	<td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left"></td>
                    <td class="text-left">Balance</td>
                    <td class="text-left">$0</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-sm-6">
                <h2>Students List</small></h2>
            </div>
            <div class="col-sm-6">
                &nbsp;
            </div>
        </div>
    </div>

    <div class="table-responsive">
        <table id="order_detail" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id">username</th>
                    <th data-column-id="fullName">Full Name</th>
                    <th data-column-id="email">Email</th>
                    <th data-column-id="phone">Phone No</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <tr data-row-id="0">
                    <td class="text-left">Parminder</td>
                    <td class="text-left">Parminder Kaur</td>
                    <td class="text-left">hgatetech@gmail.com</td>
                    <td class="text-left">9898989898</td>
                    <td class="text-left">Profile</td>
                </tr>
                <tr data-row-id="1">
                    <td class="text-left">abc</td>
                    <td class="text-left">seema</td>
                    <td class="text-left">seema@test.com</td>
                    <td class="text-left">99797979797</td>
                    <td class="text-left">Profile</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php
getfooter();
?>
<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function () {
        //Basic Example
        $("#order_detail").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {
                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/?id=" + row.id + "'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                    html += "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/addevent?id=<?php echo $id; ?>&objid=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button> ";

                    html += "<button \"deleteRecord\" onclick=\"deleteClass("+row.id+");\" type=\"button\" class=\"btn bgm-gray btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"  data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";

                    return html;
                }
            },
            converters: {
                starttime: {
                    from: function (value) {
                        var test = value.split(' From');
                        return moment(test[0]) + "T" + test[1];
                    },
                    to: function (value) {
                        var test = value.split('T:');
                        return moment.unix(test[0] / 1000).format("MMM DD,YY") + " From:" + test[1];
                    }
                }
            }
        });

    });
</script>


