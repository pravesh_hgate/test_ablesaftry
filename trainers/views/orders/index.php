<?php
getheader();
?>

    <div class="block-header">
        <div class="row">
            <div class="col-sm-6">
            	<h1>All Orders</h1>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-sm-6">
                    <h2>All Orders</h2>
                </div>
                <div class="col-sm-6">
                    &nbsp;
                </div>
            </div>
        </div>

        <div class="table-responsive">
            <table id="public_course_table" class="table table-striped table-vmiddle">
                <thead>
                    <tr>
                        <th data-column-id="id" data-type="numeric" data-order="desc">Order Id</th>
                        <th data-column-id="date">Order Date</th>
                        <th data-column-id="username">Username</th>
                        <th data-column-id="emailname">Email</th>
                        <th data-column-id="emailname">Status</th>
                        <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                    </tr>
                </thead>
                <tbody>
    				<tr data-row-id="0">
                    	<td class="text-left">001</td>
                        <td class="text-left">Dec 05, 2016</td>
                        <td class="text-left">Parminder abc</td>
                        <td class="text-left">parminder.php@gmail.com</td>
                        <td class="text-left">Pending</td>
                        <td class="text-left">
                        </td>
                    </tr>
                    <tr data-row-id="0">
                    	<td class="text-left">002</td>
                        <td class="text-left">Dec 06, 2016</td>
                        <td class="text-left">Parminder xyz</td>
                        <td class="text-left">parminder.php@gmail.com</td>
                        <td class="text-left">Paid</td>
                        <td class="text-left">
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>



<?php
getfooter();
?>

<script>
    function readURL(input) {
        if (input.files && input.files[0]) {
            $('#image_preview').show();
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#category_image')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
    $(function () {
        $("#add_course_type").validate({
            rules: {
                title: {required: true},
                cost: {required: true, digits: true}
            },
            submitHandler: function (form) {

                form.submit();

            }

        });


        $("#public_course_table").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            formatters: {
                "commands": function (column, row) {

                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/orders/orderdetail.php'\" class=\"btn btn-info btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"More info\" data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-info\"></span></button> ";

                 /*   html += "<button onclick=\"location.href='<?php echo SITE_URL;?>/courses/editcoursetype?id=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-edit\"></span></button>";*/

                    //html +=	"<button type=\"button\" class=\"btn bgm-gray btn-icon waves-effect waves-circle waves-float\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Delete\"  data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-delete\"></span></button>";

                    return html;
                }
            }
        });

    });
</script>
