<?php
 getheader();
?>
<div class="card">
                        <div class="card-header">
                            <h2>Sales Statistics <small>Orders for that past 3 months from today.</small></h2>
                            
                            <ul class="actions">
                                <li>
                                    <a href="">
                                        <i class="zmdi zmdi-refresh-alt"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="">
                                        <i class="zmdi zmdi-download"></i>
                                    </a>
                                </li>
                                <li class="dropdown">
                                    <a href="" data-toggle="dropdown">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a href="">Change Date Range</a>
                                        </li>
                                        <li>
                                            <a href="">Change Graph Type</a>
                                        </li>
                                        <li>
                                            <a href="">Other Settings</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        
                        <div class="card-body">
                            <div class="chart-edge">
                                <div id="line-chart" class="flot-chart "></div>
                            </div>
                        </div>
                    </div>
<div class="col-sm-6">
                            <!-- Calendar -->
                            <div id="calendar-widget"></div>

                    
                    

<?php
getfooter();
?>
<script>
$(document).ready(function(){
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd='0'+dd
    } 

    if(mm<10) {
        mm='0'+mm
    }
    today = yyyy+'/'+mm+'/'+dd;
    
    mm = mm - 3;
    if(mm < 1){
        mm = mm + 12;
    }
    if(mm<10) {
        mm='0'+mm
    }
    yesterday = yyyy+'/'+mm+'/'+dd;

    var options = {
        series: {
            shadowSize: 0,
            lines: {
                show: true,
                lineWidth: 0,
            },
        },
        grid: {
            borderWidth: 0,
            labelMargin:10,
            hoverable: true,
            clickable: true,
            mouseActiveRadius:6,
            
        },
        xaxis: {
            tickDecimals: 0,
            ticks: false,
            mode: "time",
            timeformat: "%Y/%m/%d",
            minTickSize: [1, "day"],
            min: (new Date(yesterday)),
            max: (new Date(today))
        },
        
        yaxis: {
            ticks: true,
            tickDecimals: 0
        },
        "lines": {"show": "true"},
        "points": {"show": "true"},
        
        legend: {
            show: false
        }
    };
    /* Make some random data for Flot Line Chart */
    var d1 = [];
    $.getJSON('<?php echo URL_PHP; ?>/orders.ajax.php', {start: yesterday, end: today}, function(data){
 
        for (i = 0; i < data.length; i++) { 
            d1.push([new Date(data[i][0]), parseInt(data[i][1])]);
        }

    /* Regular Line Chart */
    if ($("#line-chart")[0]) {
        $.plot($("#line-chart"), [
            {data: d1, lines: { show: true, fill: 0.98 }, label: 'Orders', stack: true, color: '#4285F4' }
        ], options);
    }
    /* Tooltips for Flot Charts */
    
    if ($(".flot-chart")[0]) {
        $(".flot-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                var dt = new Date(parseInt(x));
                var t = dt.toString();
                var XX = t.split(" ");
                $(".flot-tooltip").html(item.series.label + " on " + XX[1] + XX[2] + " = " + parseInt(y)).css({top: item.pageY+5, left: item.pageX+5}).show();
            }
            else {
                $(".flot-tooltip").hide();
            }
        });
        
        $("<div class='flot-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }
    });
    /*
     * Calendar Widget
     */
    if($('#calendar-widget')[0]) {
        (function(){
            $('#calendar-widget').fullCalendar({
		        contentHeight: 'auto',
		        theme: true,
                header: {
                    right: '',
                    center: 'prev, title, next',
                    left: ''
                },
                defaultDate: today,
                editable: true,
                events: [
                    {
                        title: 'All Day',
                        start: '2014-06-01',
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Long Event',
                        start: '2014-06-07',
                        end: '2014-06-10',
                        className: 'bgm-orange'
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-09',
                        className: 'bgm-lightgreen'
                    },
                    {
                        id: 999,
                        title: 'Repeat',
                        start: '2014-06-16',
                        className: 'bgm-lightblue'
                    },
                    {
                        title: 'Meet',
                        start: '2014-06-12',
                        end: '2014-06-12',
                        className: 'bgm-green'
                    },
                    {
                        title: 'Lunch',
                        start: '2014-06-12',
                        className: 'bgm-cyan'
                    },
                    {
                        title: 'Birthday',
                        start: '2014-06-13',
                        className: 'bgm-amber'
                    },
                    {
                        title: 'Google',
                        url: 'http://google.com/',
                        start: '2014-06-28',
                        className: 'bgm-amber'
                    }
                ]
            });
        })();
    }
});
</script>