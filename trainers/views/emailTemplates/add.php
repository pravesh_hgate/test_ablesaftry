<?php
$cat = array();
if ($_GET['id']) {
    $id = $_GET['id'];

    $STH = $asdb->query('SELECT * from email_templates where id =' . $id);
    $cat = $STH->fetch(PDO::FETCH_ASSOC);
}
?>
<script>
    function back() {
        location.href = '<?php echo SITE_URL;?>/emailTemplates/';
    }
</script>
<style>.validate{color:#F00;}</style>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float waves-effect waves-circle waves-float waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1> Add New Email Template </h1>
</div>
<div class="card" id="profile-main">

    <div style="padding:0px" class="pm-body clearfix">
        <form class="" id="add-email-template" method="post" >

            <div class="row">
                <div class="col-sm-12">
                    <div class="pmb-block">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-email"></i>Add New Email Template</h2>
                        </div>
                        <div class="pmbb-body p-l-30">
                            <div class="pmbb-view">
                                <dl class="">
                                    <dt>Title</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <?php if (!empty($cat)) { ?>
                                                <input name="title" id="title" class="form-control" type="text" required="true" value="<?php echo $cat['title']; ?>">
                                                <input name="id" class="form-control" type="hidden" value="<?php echo $cat['id']; ?>">
                                            <?php } else { ?>
                                                <input name="title" id="title" class="form-control" type="text" required="true" value="">
                                            <?php } ?>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="">
                                    <dt>Subject</dt>
                                    <dd>
                                        <div class="fg-line">
                                            <?php if (!empty($cat)) { ?>
                                                <input name="subject" id="subject" class="form-control" type="text" required="true" value="<?php echo $cat['subject']; ?>">
                                            <?php } else { ?>
                                                <input name="subject" id="subject" class="form-control" type="text" required="true" value="">
                                            <?php } ?>
                                        </div>
                                    </dd>
                                </dl>
                                <dl class="">
                                    <dt>Description</dt>
                                    <dd>
                                        <?php if (!empty($cat)) { ?>
                                            <textarea class="input-block-level" id="summernote" name="description"><?php echo $cat['description']; ?></textarea>
                                        <?php } else { ?>
                                            <textarea class="input-block-level" id="summernote" name="description"></textarea>
                                        <?php } ?>
                                    </dd>
                                </dl>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!--<button onclick = "submit(this)" class="btn btn-primary waves-effect" type="submit" name="addemail_template" id="addemail_template">Add Email Template</button>-->
                <button id = "addEmailTemplate" class="btn btn-primary btn-sm">Save</button>
            </div>
        </form>
    </div>
</div>


<?php
getfooter();
?>
<!-- Data Table -->
<script type="text/javascript">

    /* function submit(element) {
     var title = $('#title').val();
     var subject = $('#subject').val();
     var variables = $('#variables').val();
     if (title == '') {
     notify('Please enter Title.', 'warning');
     return false;
     }
     if (subject == '') {
     notify('Please enter Subject.', 'warning');
     return false;
     }
     if (variables == '') {
     notify('Please enter Variables.', 'warning');
     return false;
     }

     var post_data = getValues(".form-control", element);
     $.ajax({
     url: "<?php //echo URL_PHP;                  ?>/emailTemplates/add.php",
     type: "POST",
     data: post_data,
     success: function (html) {
     if (html == "true")
     {
     notify('Successfully Updated', 'success');
     } else
     {
     notify('Error updating', 'danger');
     }
     }
     });
     }

     function getValues(selector, element) {
     var tempValues = "";

     $(selector).each(function () {
     var th = $(this);
     tempValues += th.attr('name') + "=" + th.val() + "&";
     });
     tempValues += "button=" + element.id;
     return tempValues;
     }
     */

    $(document).ready(function () {

        $("#add-email-template").validate({
            rules: {
                title: {required: true},
                subject: {required: true},
                description: {required: true},
            },
            submitHandler: function (form) {

                $.ajax({
                    type: "POST",
                    url: "<?php echo URL_PHP; ?>/emailTemplates/add.php",
                    data: $('#add-email-template').serialize(),
                    beforeSend: function () {

                    },
                    success: function (response) {
                        if (response == 1)
                        {
                            location.href = '<?php echo SITE_URL;?>/emailTemplates/';
                        } else
                        {
                            /*$('#ajax_loader_div').show();
                             $('#ajax_loader').hide();
                             $('#ajax_response').html(response.message);*/
                        }
                    },
                    complete: function (data) {
                        /*$('#schedule_class').prop("disabled", false);
                         $('#ajax_loader').hide();	*/
                    }
                });
            }
        });
    });

    var counter = 1;
    function addToList()
    {
        var listElement = document.getElementById("dateList");
        var option = document.createElement("option");
        var dateElement = document.getElementById("schedule_date");
        var startTime = document.getElementById("startTime");
        var endTime = document.getElementById("endTime");
        var dateElementArray = dateElement.value.split("/");

        if (counter == 1)
        {
            document.getElementById("first_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
            document.getElementById("first_starttime").value = startTime.value;
            document.getElementById("last_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
        } else
        {
            document.getElementById("last_startdate").value = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];
        }

        var dateElementArray = dateElement.value.split("/");

        var fixedDateString = dateElementArray[2] + "-" + dateElementArray[1] + "-" + dateElementArray[0];

        var dateobj = new Date(fixedDateString);
        var dateString = dateobj.toUTCString();
        var dateArray = dateString.split(" ");

        var dateTextForm = dateArray[2] + " " + dateArray[1] + ", " + dateArray[3] + " From: " + startTime.value +
                " To: " + endTime.value + "  ";

        option.text = dateTextForm;
        option.value = dateTextForm;

        listElement.add(option);
        counter++;

    }


    $('#addDate').click(function () {
        addToList();
        return false;
    });

    $('#removeFromList').click(function () {

        var listElement = document.getElementById("dateList");
        listElement.remove(listElement.selectedIndex);
    });


    function getValues(selector, element)
    {
        var tempValues = "";
        var listElement = document.getElementById("dateList");

        for (var i = 0; i < listElement.options.length; i++)
        {
            listElement.options[i].selected = true;
        }

        $(selector).each(function () {
            var th = $(this);
            tempValues += th.attr('name') + "=" + th.val() + "&";

        });

        tempValues += "button=" + element.id + "&cat=<?php echo $_GET['id']; ?>";
        return tempValues;
    }


</script>
