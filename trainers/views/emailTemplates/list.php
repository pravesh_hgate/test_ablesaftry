<?php
$STH = $asdb->query('SELECT * FROM email_templates');
?>
<div class="block-header">
    <button onclick="back()" class="pull-left btn btn-danger btn-icon waves-effect waves-circle waves-float"><i class="zmdi zmdi-arrow-back"></i></button>
    <h1><?php echo "Email Templates"; ?></h1>
</div>
<div class="card">
    <div class="card-header">
        <div class="row">
            <div class = "col-sm-6">
                <h2>Email Templates</h2>
            </div>
            <div class = "col-sm-6">
                <a data-toggle="modal" href="<?php echo SITE_URL;?>/emailTemplates/?create=add" class="btn btn-sm btn-default waves-effect bgm-blue" style="float:right;">Add Email Template</a>
            </div>
        </div>
    </div>
    <div class="table-responsive table-editable">
        <table id="data-table-selection" class="table table-striped table-vmiddle">
            <thead>
                <tr>
                    <th data-column-id="id" data-type="numeric" data-order="desc">ID</th>
                    <th data-column-id="title">Title</th>
                    <th data-column-id="subject">Subject</th>
                    <th data-column-id="status"   data-visible="false">Subject</th>
                    <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>
                </tr>
            </thead>
            <tbody>
                <?php
                while ($rows = $STH->fetch(PDO::FETCH_ASSOC)) {
                    $cat_div .= '<tr>';
                    $cat_div .= '<td>' . $rows['id'] . '</td>';
                    $cat_div .= '<td>' . $rows['title'] . '</td>';
                    $cat_div .= '<td>' . $rows['subject'] . '</td>';
                    $cat_div .= '<td>' . $rows['status'] . '</td>';
                    $cat_div .= '</tr>';
                }
                echo $cat_div;
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php getfooter(); ?>
<style>

    .edit{
        text-decoration: none;
        border: 0px;
        color: rgb(94,94,94);
    }
    #invitations{
        display: none;
    }
</style>
<!-- Data Table -->
<script type="text/javascript">
    $(document).ready(function () {

        $.fn.editable.defaults.mode = 'inline';
        //Selection
        var grid = $("#data-table-selection").bootgrid({
            css: {
                icon: 'zmdi icon',
                iconColumns: 'zmdi-view-module',
                iconDown: 'zmdi-expand-more',
                iconRefresh: 'zmdi-refresh',
                iconUp: 'zmdi-expand-less'
            },
            selection: true,
            multiSelect: true,
            rowSelect: true,
            keepSelection: true,
            formatters: {
                "commands": function (column, row) {
                    if (row.status == "1") {
                        var class1 = "bgm-orange";
                    } else {
                        var class1 = "bgm-gray";
                    }
                    var html = "<button onclick=\"location.href='<?php echo SITE_URL;?>/emailTemplates/?create=add&id=" + row.id + "'\" type=\"button\" class=\"btn bgm-lightgreen btn-icon waves-effect waves-circle waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Edit\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-more-vert\"></span></button> "
                            + "<button type=\"button\" rel=\""+row.status+"\" class=\"btn " + class1 + " btn-icon waves-effect waves-circle status waves-float\"  data-toggle=\"tooltip\" data-placement=\"top\" title=\"Status\"   data-row-id=\"" + row.id + "\"><span class=\"zmdi zmdi-check\"></span></button> ";
                    return html;
                }

            }
        }).on("loaded.rs.jquery.bootgrid", function (e) {
            $('.edit').each(function (column, row) {
                $(this).editable(
                        );
            });
            grid.find(".status").on("click", function (column, row) {

                if($(this).attr('rel')=="1"){var status1="0";}else{var status1="1";}
                $.ajax({
                    url: "<?php echo URL_PHP; ?>/emailTemplates/status.php",
                    type: "POST",
                    data: {changeStatus:1,id:$(this).data('row-id'),'status':status1},
                    context:this,
                    success: function (html) {
                        console.info($(this))
                        $(this).attr('rel',html)
                        if (html == "1")
                        {
                            $(this).removeClass('bgm-gray');
                            $(this).addClass('bgm-orange');
                            notify('Email template activated successfully.', 'success');
                        } else {
                            $(this).removeClass('bgm-orange');
                            $(this).addClass('bgm-gray');
                            notify('Email template inactivated successfully.', 'danger');
                        }

                    }
                });

            })
        });

    });
    var curForm;
    function showForm(formId) {
        if (curForm != null) {
            curForm.hide();
            curForm = null;
        } else {
            curForm = $(formId);
            curForm.show();
        }
    }
    function submit(element) {

        var post_data = getValues(".form-control", element);

        $.ajax({
            url: "<?php echo URL_PHP; ?>/instructor/invite.ajax.php",
            type: "POST",
            data: post_data,
            success: function (html) {
                if (html == "true")
                {
                    notify('Invite Sent', 'success');
                } else {
                    notify('Error Sending Invite', 'danger');
                }

            }
        });
    }

    function getValues(selector, element) {
        var tempValues = "";

        $(selector).each(function () {
            var th = $(this);
            tempValues += th.attr('name') + "=" + th.val() + "&";

        });
        tempValues += "button=" + element.id;
        return tempValues;
    }
</script>